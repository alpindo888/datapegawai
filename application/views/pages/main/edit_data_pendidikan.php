<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="activity"></i></div>
                    <span>Edit Data Pendidikan</span>
                </h1>
                <div class="page-header-subtitle">Mendedit Data Pendidikan</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header">Edit Data Pendidikan</div>
                    <div class="card-body">
                        <div class="datatable table-responsive">
                            <form method="post">
                                <div class="row mt-5">
                                    <div class="col-12 text-center">
                                        <h3>Data Pendidikan</h3>
                                    </div>
                                </div>
                                <input type="hidden" name="id_pendidikan" value="<?= $getdata['id_pendidikan'] ?>">
                                <input type="hidden" name="nik" id="nik" value="<?= $getdata['nik'] ?>">
                                <div class="form-group">
                                    <label for="jenjang">Jenjang</label>
                                    <input autocomplete="off" type="text" class="form-control" name="jenjang" id="jenjang" placeholder="Masukan Jenjang" value="<?= $getdata["jenjang"] ?>">
                                <?= form_error('jenjang', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="nama_sekolah_universitas">Nama Sekolah / Universitas</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama_sekolah_universitas" id="nama_sekolah_universitas" placeholder="Masukan nama sekolah / universitas" value="<?= $getdata["nama_sekolah_universitas"] ?>">
                                <?= form_error('nama_sekolah_universitas', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="fakultas">Fakultas</label>
                                    <input autocomplete="off" type="text" class="form-control" name="fakultas" id="fakultas" placeholder="Masukan fakultas" value="<?= $getdata["fakultas"] ?>">
                                <?= form_error('fakultas', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="jurusan">Jurusan</label>
                                    <input autocomplete="off" type="text" class="form-control" name="jurusan" id="jurusan" placeholder="Masukan jurusan" value="<?= $getdata["jurusan"] ?>">
                                <?= form_error('jurusan', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tahun_masuk">Tahun Masuk</label>
                                    <input autocomplete="off" type="text" class="form-control" name="tahun_masuk" id="tahun_masuk" placeholder="Masukan tahun masuk" value="<?= $getdata["tahun_masuk"] ?>">
                                <?= form_error('tahun_masuk', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tahun_lulus">Tahun Lulus</label>
                                    <input autocomplete="off" type="text" class="form-control" name="tahun_lulus" id="tahun_lulus" placeholder="Masukan tahun lulus" value="<?= $getdata["tahun_lulus"] ?>">
                                <?= form_error('tahun_lulus', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>

                                <a href="<?= base_url("karyawan/view/").base64_encode($getdata['nik']) ?>" class="btn btn-danger">Cancel</a>
                                <button class="btn btn-success">Edit Data</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

