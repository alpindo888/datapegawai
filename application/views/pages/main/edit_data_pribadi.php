<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="activity"></i></div>
                    <span>Edit Data Pribadi</span>
                </h1>
                <div class="page-header-subtitle">Mendedit Data Pribadi</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header">Edit Data Pribadi</div>
                    <div class="card-body">
                        <div class="datatable table-responsive">
                            <?= form_open_multipart("karyawan/edit_data_pribadi/".base64_encode($getdata['nik'])); ?>
                            <input type="hidden" name="id_pendidikan" value="<?= $pendidikan['id_pendidikan'] ?>">
                            <input type="hidden" name="id_datakeluarga" value="<?= $keluarga['id_datakeluarga'] ?>">
                            <input type="hidden" name="id_pekerjaan" value="<?= $pekerjaan['id_pekerjaan'] ?>">
                            <input type="hidden" name="id_pribadi" value="<?= $getdata['id_pribadi'] ?>">
                            <input type="hidden" name="kode_pegawai" value="<?= $getdata['kode_pegawai'] ?>">
                            <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama" id="nama" placeholder="Masukan Nama" value="<?= $getdata["nama"] ?>">
                                <?= form_error('nama', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="nuptk">NUPTK</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nuptk" id="nuptk" placeholder="Masukan NUPTK" value="<?= $getdata["nuptk"] ?>">
                                <?= form_error('nuptk', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="nik">NIK</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nik" id="nik" placeholder="Masukan NIK" value="<?= $getdata["nik"] ?>">
                                <?= form_error('nik', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="jenis_kelamin">Jenis Kelamin</label>
                                    <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                                        <?php foreach($jenis_kelamin as $value): ?>
                                            <?php if($getdata['jenis_kelamin'] == $value){ ?>
                                                <option value="<?= $value ?>" checked><?= $value ?></option>
                                            <?php } else { ?>
                                                <option value="<?= $value ?>"><?= $value ?></option>
                                            <?php } ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="tempat_lahir">Tempat Lahir</label>
                                    <input autocomplete="off" type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" placeholder="Masukan tempat lahir" value="<?= $getdata["tempat_lahir"] ?>">
                                <?= form_error('tempat_lahir', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_lahir">Tanggal Lahir</label>
                                    <input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir" value="<?= $getdata["tanggal_lahir"] ?>">
                                <?= form_error('tanggal_lahir', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="status_kepegawaian">Status Pegawai</label>
                                    <select class="form-control" name="status_kepegawaian" id="status_kepegawaian">
                                        <?php foreach($status_kepegawaian as $val): ?>
                                            <?php if($getdata['status_kepegawaian'] == $val ){ ?>
                                                <option value="<?= $val ?>" checked><?= $val ?></option>
                                            <?php } else { ?>
                                                <option value="<?= $val ?>"><?= $val ?></option>
                                            <?php } ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="jabatan">Jabatan</label>
                                    <select class="form-control" name="jabatan" id="jabatan">
                                    <?php foreach($jabatan as $val): ?>
                                            <?php if($getdata['jabatan'] == $val ){ ?>
                                                <option value="<?= $val ?>" checked><?= $val ?></option>
                                            <?php } else { ?>
                                                <option value="<?= $val ?>"><?= $val ?></option>
                                            <?php } ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="agama">Agama</label>
                                    <input autocomplete="off" type="text" class="form-control" name="agama" id="agama" placeholder="Masukan agama" value="<?= $getdata["agama"] ?>">
                                <?= form_error('agama', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <textarea type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukan alamat"><?= $getdata["alamat"] ?></textarea>
                                <?= form_error('alamat', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="no_hp">NO HP</label>
                                    <input autocomplete="off" type="text" class="form-control" name="no_hp" id="no_hp" placeholder="Masukan no hp" value="<?= $getdata["no_hp"] ?>">
                                <?= form_error('no_hp', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <?php if($role == 'user'){ ?>
                                    <div class="form-group">
                                        <input autocomplete="off" type="hidden" class="form-control" name="email" id="email" placeholder="Masukan email" value="<?= $email ?>">
                                    <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                <?php }else{ ?>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input autocomplete="off" type="text" class="form-control" name="email" id="email" placeholder="Masukan email" value="<?= $getdata["email"] ?>">
                                    <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                <?php } ?>
                                <div class="form-group">
                                    <label for="tmt">TMT</label>
                                    <input type="date" class="form-control" name="tmt" id="tmt" value="<?= $getdata["tmt"] ?>">
                                <?= form_error('tmt', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="nama_ibu_kandung">Nama Ibu Kandung</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama_ibu_kandung" id="nama_ibu_kandung" placeholder="Masukan nama ibu kandung" value="<?= $getdata["nama_ibu_kandung"] ?>">
                                <?= form_error('nama_ibu_kandung', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="status_perkawinan">Status Perkawinan</label>
                                    <input autocomplete="off" type="text" class="form-control" name="status_perkawinan" id="status_perkawinan" placeholder="Masukan status perkawinan" value="<?= $getdata["status_perkawinan"] ?>">
                                <?= form_error('status_perkawinan', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="nama_suami_istri">Nama Suami Istri</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama_suami_istri" id="nama_suami_istri" placeholder="Masukan nama suami / istri" value="<?= $getdata["nama_suami_istri"] ?>">
                                <?= form_error('nama_suami_istri', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="pekerjaan_suami_istri">Pekerjaan Suami Istri</label>
                                    <input autocomplete="off" type="text" class="form-control" name="pekerjaan_suami_istri" id="pekerjaan_suami_istri" placeholder="Masukan pekerjaan suami / istri" value="<?= $getdata["pekerjaan_suami_istri"] ?>">
                                <?= form_error('pekerjaan_suami_istri', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="jumlah_anak">Jumlah Anak</label>
                                    <input autocomplete="off" type="number" class="form-control" name="jumlah_anak" id="jumlah_anak" placeholder="Masukan Jumlah Anak" value="<?= $getdata["jumlah_anak"] ?>">
                                <?= form_error('jumlah_anak', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="jumlah_saudara">Jumlah Saudara</label>
                                    <input autocomplete="off" type="number" class="form-control" name="jumlah_saudara" id="jumlah_saudara" placeholder="Masukan Jumlah Saudara" value="<?= $getdata["jumlah_saudara"] ?>">
                                <?= form_error('jumlah_saudara', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="npwp">NPWP</label>
                                    <input autocomplete="off" type="text" class="form-control" name="npwp" id="npwp" placeholder="Masukan npwp" value="<?= $getdata["npwp"] ?>">
                                <?= form_error('npwp', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="nama_wajib_pajak">Nama Wajib Pajak</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama_wajib_pajak" id="nama_wajib_pajak" placeholder="Masukan nama wajib pajak" value="<?= $getdata["nama_wajib_pajak"] ?>">
                                <?= form_error('nama_wajib_pajak', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="kewarganegaraan">Kewarganegaraan</label>
                                    <input autocomplete="off" type="text" class="form-control" name="kewarganegaraan" id="kewarganegaraan" placeholder="Masukan kewarganegaraan" value="<?= $getdata["kewarganegaraan"] ?>">
                                <?= form_error('kewarganegaraan', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group row mb-5">
                                    <div class="col-sm-12">Picture</div>
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <img src="<?= base_url('assets/img/img_user/') . $getdata['image']; ?>" class="img-thumbnail">
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="image" name="image">
                                                    <label class="custom-file-label" for="image">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="<?= base_url("karyawan/view/").base64_encode($getdata['nik']) ?>" class="btn btn-danger">Cancel</a>
                                <button class="btn btn-success">Edit Data</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

