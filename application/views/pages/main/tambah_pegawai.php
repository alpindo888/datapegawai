 <main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="activity"></i></div>
                    <span>Tambah Pegawai</span>
                </h1>
                <div class="page-header-subtitle">Tambahkan Data Pegawai</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header">Tambah Pegawai</div>
                    <div class="card-body">
                        <p class="text-danger" style="font-size:11px;">*isi dengan (0) apabila value tidak ada</p>
                        <div class="datatable table-responsive">
                            <?= form_open_multipart("karyawan/tambah"); ?>
                                <div class="form-group">
                                    <label for="kode_pegawai">Kode Pegawai</label>
                                    <input autocomplete="off" type="text" class="form-control mb-2" name="kode_pegawai" id="kode_pegawai" placeholder="Masukan Nama" readonly value="<?= $kodePegawai ?>">
                                <?= form_error('kode_pegawai', '<small class="text-danger pl-3">', '</small>'); ?>
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama" id="nama" placeholder="Masukan Nama" value="<?= set_value("nama") ?>">
                                <?= form_error('nama', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="nuptk">NUPTK</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nuptk" id="nuptk" placeholder="Masukan NUPTK" value="<?= set_value("nuptk") ?>">
                                <?= form_error('nuptk', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="nik">NIK</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nik" id="nik" placeholder="Masukan NIK" value="<?= set_value("nik") ?>">
                                <?= form_error('nik', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="jenis_kelamin">Jenis Kelamin</label>
                                    <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                                        <option value="Laki-laki">Laki-laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="tempat_lahir">Tempat Lahir</label>
                                    <input autocomplete="off" type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" placeholder="Masukan tempat lahir" value="<?= set_value("tempat_lahir") ?>">
                                <?= form_error('tempat_lahir', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_lahir">Tanggal Lahir</label>
                                    <input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir" value="<?= set_value("tanggal_lahir") ?>">
                                <?= form_error('tanggal_lahir', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="status_kepegawaian">Status Pegawai</label>
                                    <select class="form-control" name="status_kepegawaian" id="status_kepegawaian">
                                        <option value="GTY">GTY</option>
                                        <option value="GTT">GTT</option>
                                        <option value="Honor">Honor</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="jabatan">Jabatan</label>
                                    <select class="form-control" name="jabatan" id="jabatan">
                                        <option value="Guru">Guru</option>
                                        <option value="Kepala Sekolah">Kepala Sekolah</option>
                                        <option value="Tenaga Administrasi">Tenaga Administrasi</option>
                                        <option value="Keamanan">Keamanan</option>
                                        <option value="Tenaga Kebersihan">Tenaga Kebersihan</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="agama">Agama</label>
                                    <input autocomplete="off" type="text" class="form-control" name="agama" id="agama" placeholder="Masukan agama" value="<?= set_value("agama") ?>">
                                <?= form_error('agama', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <textarea type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukan alamat"><?= set_value("alamat") ?></textarea>
                                <?= form_error('alamat', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="no_hp">NO HP</label>
                                    <input autocomplete="off" type="text" class="form-control" name="no_hp" id="no_hp" placeholder="Masukan no hp" value="<?= set_value("no_hp") ?>">
                                <?= form_error('no_hp', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <?php if($role == 'user'){ ?>
                                    <div class="form-group">
                                        <input autocomplete="off" type="hidden" class="form-control" name="email" id="email" placeholder="Masukan email" value="<?= $email ?>">
                                    <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                <?php }else{ ?>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input autocomplete="off" type="text" class="form-control" name="email" id="email" placeholder="Masukan email" value="<?= set_value("email") ?>">
                                    <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                <?php } ?>
                                <div class="form-group">
                                    <label for="tmt">TMT</label>
                                    <input type="date" class="form-control" name="tmt" id="tmt" value="<?= set_value("tmt") ?>">
                                <?= form_error('tmt', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="nama_ibu_kandung">Nama Ibu Kandung</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama_ibu_kandung" id="nama_ibu_kandung" placeholder="Masukan nama ibu kandung" value="<?= set_value("nama_ibu_kandung") ?>">
                                <?= form_error('nama_ibu_kandung', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="status_perkawinan">Status Perkawinan</label>
                                    <input autocomplete="off" type="text" class="form-control" name="status_perkawinan" id="status_perkawinan" placeholder="Masukan status perkawinan" value="<?= set_value("status_perkawinan") ?>">
                                <?= form_error('status_perkawinan', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="nama_suami_istri">Nama Suami Istri</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama_suami_istri" id="nama_suami_istri" placeholder="Masukan nama suami / istri" value="<?= set_value("nama_suami_istri") ?>">
                                <?= form_error('nama_suami_istri', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="pekerjaan_suami_istri">Pekerjaan Suami Istri</label>
                                    <input autocomplete="off" type="text" class="form-control" name="pekerjaan_suami_istri" id="pekerjaan_suami_istri" placeholder="Masukan pekerjaan suami / istri" value="<?= set_value("pekerjaan_suami_istri") ?>">
                                <?= form_error('pekerjaan_suami_istri', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="jumlah_anak">Jumlah Anak</label>
                                    <input autocomplete="off" type="number" class="form-control" name="jumlah_anak" id="jumlah_anak" placeholder="Masukan Jumlah Anak" value="<?= set_value("jumlah_anak") ?>">
                                <?= form_error('jumlah_anak', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="jumlah_saudara">Jumlah Saudara</label>
                                    <input autocomplete="off" type="number" class="form-control" name="jumlah_saudara" id="jumlah_saudara" placeholder="Masukan Jumlah Saudara" value="<?= set_value("jumlah_saudara") ?>">
                                <?= form_error('jumlah_saudara', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="npwp">NPWP</label>
                                    <input autocomplete="off" type="text" class="form-control" name="npwp" id="npwp" placeholder="Masukan npwp" value="<?= set_value("npwp") ?>">
                                <?= form_error('npwp', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="nama_wajib_pajak">Nama Wajib Pajak</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama_wajib_pajak" id="nama_wajib_pajak" placeholder="Masukan nama wajib pajak" value="<?= set_value("nama_wajib_pajak") ?>">
                                <?= form_error('nama_wajib_pajak', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="kewarganegaraan">Kewarganegaraan</label>
                                    <input autocomplete="off" type="text" class="form-control" name="kewarganegaraan" id="kewarganegaraan" placeholder="Masukan kewarganegaraan" value="<?= set_value("kewarganegaraan") ?>">
                                <?= form_error('kewarganegaraan', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="images">Upload Foto</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="image" name="image">
                                        <label class="custom-file-label" for="image">Pilih file</label>
                                    </div>
                                    <?= form_error('image', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="row mt-5">
                                    <div class="col-12 text-center">
                                        <h3>Data Suami / Istri</h3>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama_suami">Nama Suami / Istri</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama_suami" id="nama_suami" placeholder="Masukan nama suami / istri" value="<?= set_value("nama_suami") ?>" readonly>
                                <?= form_error('nama_suami', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tempat_lahir_suami">Tempat Lahir</label>
                                    <input autocomplete="off" type="text" class="form-control" name="tempat_lahir_suami" id="tempat_lahir_suami" placeholder="Masukan tempat lahir" value="<?= set_value("tempat_lahir_suami") ?>">
                                <?= form_error('tempat_lahir_suami', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_lahir_suami">Tanggal Lahir</label>
                                    <input type="date" class="form-control" name="tanggal_lahir_suami" id="tanggal_lahir_suami" value="<?= set_value("tanggal_lahir_suami") ?>">
                                <?= form_error('tanggal_lahir_suami', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="pekerjaan_suami">Pekerjaan</label>
                                    <input autocomplete="off" type="text" class="form-control" name="pekerjaan_suami" id="pekerjaan_suami" placeholder="Masukan pekerjaan" value="<?= set_value("pekerjaan_suami") ?>" readonly>
                                <?= form_error('pekerjaan_suami', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="row mt-5">
                                    <div class="col-12 text-center">
                                        <h3>Data Ibu</h3>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama_ibu">Nama Ibu</label>
                                    <input readonly autocomplete="off" type="text" class="form-control" name="nama_ibu" id="nama_ibu" placeholder="Masukan nama ibu" value="<?= set_value("nama_ibu") ?>">
                                <?= form_error('nama_ibu', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tempat_lahir_ibu">Tempat Lahir</label>
                                    <input autocomplete="off" type="text" class="form-control" name="tempat_lahir_ibu" id="tempat_lahir_ibu" placeholder="Masukan tempat lahir" value="<?= set_value("tempat_lahir_ibu") ?>">
                                <?= form_error('tempat_lahir_ibu', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_lahir_ibu">Tanggal Lahir</label>
                                    <input type="date" class="form-control" name="tanggal_lahir_ibu" id="tanggal_lahir_ibu" value="<?= set_value("tanggal_lahir_ibu") ?>">
                                <?= form_error('tanggal_lahir_ibu', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="pekerjaan_ibu">Pekerjaan Ibu</label>
                                    <input autocomplete="off" type="text" class="form-control" name="pekerjaan_ibu" id="pekerjaan_ibu" placeholder="Masukan pekerjaan ibu" value="<?= set_value("pekerjaan_ibu") ?>">
                                <?= form_error('pekerjaan_ibu', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="row mt-5">
                                    <div class="col-12 text-center">
                                        <h3>Data Ayah</h3>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama_ayah">Nama ayah</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama_ayah" id="nama_ayah" placeholder="Masukan nama ayah" value="<?= set_value("nama_ayah") ?>">
                                <?= form_error('nama_ayah', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tempat_lahir_ayah">Tempat Lahir</label>
                                    <input autocomplete="off" type="text" class="form-control" name="tempat_lahir_ayah" id="tempat_lahir_ayah" placeholder="Masukan tempat lahir" value="<?= set_value("tempat_lahir_ayah") ?>">
                                <?= form_error('tempat_lahir_ayah', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_lahir_ayah">Tanggal Lahir</label>
                                    <input type="date" class="form-control" name="tanggal_lahir_ayah" id="tanggal_lahir_ayah" value="<?= set_value("tanggal_lahir_ayah") ?>">
                                <?= form_error('tanggal_lahir_ayah', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="pekerjaan_ayah">Pekerjaan ayah</label>
                                    <input autocomplete="off" type="text" class="form-control" name="pekerjaan_ayah" id="pekerjaan_ayah" placeholder="Masukan pekerjaan ayah" value="<?= set_value("pekerjaan_ayah") ?>">
                                <?= form_error('pekerjaan_ayah', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="row mt-5">
                                    <div class="col-12 text-center">
                                        <h3>Data Riwayat Pekerjaan</h3>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="dari_bulan_tahun">Dari</label>
                                    <input type="date" class="form-control" name="dari_bulan_tahun" id="dari_bulan_tahun" value="<?= set_value("dari_bulan_tahun") ?>">
                                <?= form_error('dari_bulan_tahun', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="sampai_bulan_tahun">Sampai</label>
                                    <input type="date" class="form-control" name="sampai_bulan_tahun" id="sampai_bulan_tahun" value="<?= set_value("sampai_bulan_tahun") ?>">
                                <?= form_error('sampai_bulan_tahun', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="nama_perusahaan">Nama Perusahaan</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama_perusahaan" id="nama_perusahaan" placeholder="Masukan nama perusahaan" value="<?= set_value("nama_perusahaan") ?>">
                                <?= form_error('nama_perusahaan', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="alamat_perusahaan">Alamat Perusahaan</label>
                                    <input autocomplete="off" type="text" class="form-control" name="alamat_perusahaan" id="alamat_perusahaan" placeholder="Masukan alamat perusahaan" value="<?= set_value("alamat_perusahaan") ?>">
                                <?= form_error('alamat_perusahaan', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="row mt-5">
                                    <div class="col-12 text-center">
                                        <h3>Data Pendidikan</h3>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="jenjang">Jenjang</label>
                                    <input autocomplete="off" type="text" class="form-control" name="jenjang" id="jenjang" placeholder="Masukan Jenjang" value="<?= set_value("jenjang") ?>">
                                <?= form_error('jenjang', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="nama_sekolah_universitas">Nama Sekolah / Universitas</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama_sekolah_universitas" id="nama_sekolah_universitas" placeholder="Masukan nama sekolah / universitas" value="<?= set_value("nama_sekolah_universitas") ?>">
                                <?= form_error('nama_sekolah_universitas', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="fakultas">Fakultas</label>
                                    <input autocomplete="off" type="text" class="form-control" name="fakultas" id="fakultas" placeholder="Masukan fakultas" value="<?= set_value("fakultas") ?>">
                                <?= form_error('fakultas', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="jurusan">Jurusan</label>
                                    <input autocomplete="off" type="text" class="form-control" name="jurusan" id="jurusan" placeholder="Masukan jurusan" value="<?= set_value("jurusan") ?>">
                                <?= form_error('jurusan', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tahun_masuk">Tahun Masuk</label>
                                    <input autocomplete="off" type="text" class="form-control" name="tahun_masuk" id="tahun_masuk" placeholder="Masukan tahun masuk" value="<?= set_value("tahun_masuk") ?>">
                                <?= form_error('tahun_masuk', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tahun_lulus">Tahun Lulus</label>
                                    <input autocomplete="off" type="text" class="form-control" name="tahun_lulus" id="tahun_lulus" placeholder="Masukan tahun lulus" value="<?= set_value("tahun_lulus") ?>">
                                <?= form_error('tahun_lulus', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <button class="btn btn-success">Tambah Data</button>
                                <a href="<?= base_url("karyawan") ?>" class="btn btn-danger">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
$(document).ready(function(){
  $("#nama_suami_istri").keyup(function(){
  	var val = $("#nama_suami_istri").val();
    $("#nama_suami").val(val);
  });
  $("#pekerjaan_suami_istri").keyup(function(){
  	var val = $("#pekerjaan_suami_istri").val();
    $("#pekerjaan_suami").val(val);
  });
  $("#nama_ibu_kandung").keyup(function(){
  	var val = $("#nama_ibu_kandung").val();
    $("#nama_ibu").val(val);
  });
});
</script>


