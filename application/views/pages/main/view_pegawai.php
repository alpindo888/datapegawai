<style>
    .pdf{
        position: absolute;
        top: 200px;
        z-index: 2;
        right: 40px;
    }
</style>


<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="activity"></i></div>
                    <span>Lihat Detail</span>
                </h1>
                <div class="page-header-subtitle">Lihat Detail Pegawai.</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="card mb-4">
        <?= $this->session->flashdata('pribadi'); ?>
            <div class="card-header">Data Pegawai</div>
            <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <h5>Kode Pegawai</h5>
                    <h5>Nama</h5>
                    <h5>NUPTK</h5>
                    <h5>NIK </h5>
                    <h5>Jenis Kelamin</h5>
                    <h5>Tempat Lahir</h5>
                    <h5>Tanggal Lahir</h5>
                    <h5>Status Kepegawaian</h5>
                    <h5>Jabatan</h5>
                    <h5>Agama</h5>
                    <h5>Alamat</h5>
                    <h5>NO HP</h5>
                    <h5>Email</h5>
                    <h5>Tmt</h5>
                    <h5>Nama Ibu Kandung</h5>
                    <h5>Status Perkawinan</h5>
                    <h5>Nama Suami / Istri</h5>
                    <h5>Pekerjaan Suami / Istri</h5>
                    <h5>Jumlah Anak</h5>
                    <h5>Jumlah Saudara</h5>
                    <h5>NPWP</h5>
                    <h5>Nama Wajib Pajak</h5>
                    <h5>Kewarganegaraan</h5>
                    <h5>Foto</h5>

                </div>
                <div class="col-1">
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                </div>
                <div class="col-8">
                    <h5><?= $query[0]['kode_pegawai'] ?></h5>
                    <h5><?= $query[0]['nama'] ?></h5>
                    <h5><?= $query[0]['nuptk'] ?></h5>
                    <h5><?= $query[0]['nik'] ?></h5>
                    <h5><?= $query[0]['jenis_kelamin'] ?></h5>
                    <h5><?= $query[0]['tempat_lahir'] ?></h5>
                    <h5><?= date("d-m-Y", strtotime($query[0]['tanggal_lahir'])) ?></h5>
                    <h5><?= $query[0]['status_kepegawaian'] ?></h5>
                    <h5><?= $query[0]['jabatan'] ?></h5>
                    <h5><?= $query[0]['agama'] ?></h5>
                    <h5><?= $query[0]['alamat'] ?></h5>
                    <h5><?= $query[0]['no_hp'] ?></h5>
                    <h5><?= $query[0]['email'] ?></h5>
                    <h5><?= $query[0]['tmt'] ?></h5>
                    <h5><?= $query[0]['nama_ibu_kandung'] ?></h5>
                    <h5><?= $query[0]['status_perkawinan'] ?></h5>
                    <h5><?= $query[0]['nama_suami_istri'] ?></h5>
                    <h5><?= $query[0]['pekerjaan_suami_istri'] ?></h5>
                    <h5><?= $query[0]['jumlah_anak'] ?></h5>
                    <h5><?= $query[0]['jumlah_saudara'] ?></h5>
                    <h5><?= $query[0]['npwp'] ?></h5>
                    <h5><?= $query[0]['nama_wajib_pajak'] ?></h5>
                    <h5><?= $query[0]['kewarganegaraan'] ?></h5>
                    <img src="<?= base_url('assets/img/img_user/').$query[0]['image'] ?>" alt="" style="width:70px; height:70px;">
                </div>
            </div>
            <?php if($role != "super_admin"){ ?>
            <div class="row">
                <div class="col-12 text-center mt-4">
                    <a href="<?= base_url("karyawan/edit_data_pribadi/").base64_encode($query[0]['nik']) ?>" class="btn btn-primary">Edit Data</a>
                </div>
            </div>
            <?php } ?>
            
            </div>
        </div>

        <div class="card mb-4">
        <?= $this->session->flashdata('keluarga'); ?>
            <div class="card-header">Data Keluarga</div>
            <div class="card-body">
            <div class="row">
                <div class="col-4">
                    <h5>Nama Suami / Istri</h5>
                    <h5>Tempat Lahir Suami / Istri</h5>
                    <h5>Tanggal Lahir Suami / Istri </h5>
                    <h5>Pekerjaan Suami / Istri</h5>
                    <h5>Nama Ibu</h5>
                    <h5>Tempat Lahir Ibu</h5>
                    <h5>Tanggal Lahir Ibu</h5>
                    <h5>Pekerjaan Ibu</h5>
                    <h5>Nama Ayah</h5>
                    <h5>Tempat Lahir Ayah</h5>
                    <h5>Tanggal Lahir Ayah</h5>
                    <h5>Pekerjaan Ayah</h5>
                </div>
                <div class="col-1">
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                </div>
                <div class="col-7">
                    <h5><?= $query[0]['nama_suami'] ?></h5>
                    <h5><?= $query[0]['tempat_lahir_suami'] ?></h5>
                    <h5><?= ($query[0]['tanggal_lahir_suami'] == "") ? "0" : date("d-m-Y", strtotime($query[0]['tanggal_lahir_suami'])) ?></h5>
                    <h5><?= $query[0]['pekerjaan_suami'] ?></h5>
                    <h5><?= $query[0]['nama_ibu'] ?></h5>
                    <h5><?= $query[0]['tempat_lahir_ibu'] ?></h5>
                    <h5><?= date("d-m-Y", strtotime($query[0]['tanggal_lahir_ibu'])) ?></h5>
                    <h5><?= $query[0]['pekerjaan_ibu'] ?></h5>
                    <h5><?= $query[0]['nama_ayah'] ?></h5>
                    <h5><?= $query[0]['tempat_lahir_ayah'] ?></h5>
                    <h5><?= date("d-m-Y", strtotime($query[0]['tanggal_lahir_ayah'])) ?></h5>
                    <h5><?= $query[0]['pekerjaan_ayah'] ?></h5>
                </div>
            </div>
            <?php if($role != "super_admin"){ ?>
            <div class="row">
                <div class="col-12 text-center mt-4">
                    <a href="<?= base_url("karyawan/edit_data_keluarga/").base64_encode($query[0]['nik']) ?>" class="btn btn-primary">Edit Data</a>
                </div>
            </div>
            <?php } ?>
            
            </div>
        </div>
        <div class="card mb-4">
        <?= $this->session->flashdata('pekerjaan'); ?>
            <div class="card-header">Data Riwayat Pekerjaan</div>
            <div class="card-body">
            <div class="row">
                <div class="col-4">
                    <h5>Dari</h5>
                    <h5>Sampai</h5>
                    <h5>Nama Perusahaan</h5>
                    <h5>Alamat Perusahaan</h5>
                </div>
                <div class="col-1">
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                </div>
                <div class="col-7">
                    <h5><?= ($query[0]['dari_bulan_tahun'] == "") ? "0" : date("d-m-Y", strtotime($query[0]['dari_bulan_tahun'])) ?></h5>
                    <h5><?= ($query[0]['sampai_bulan_tahun'] == "") ? "0" : date("d-m-Y", strtotime($query[0]['sampai_bulan_tahun'])) ?></h5>
                    <h5><?= $query[0]['nama_perusahaan'] ?></h5>
                    <h5><?= $query[0]['alamat_perusahaan'] ?></h5>
                </div>
            </div>
            <?php if($role != "super_admin"){ ?>
            <div class="row">
                <div class="col-12 text-center mt-4">
                    <a href="<?= base_url("karyawan/edit_data_pekerjaan/").base64_encode($query[0]['nik']) ?>" class="btn btn-primary">Edit Data</a>
                </div>
            </div>
            <?php } ?>
            
            </div>
        </div>
        <div class="card mb-4">
        <?= $this->session->flashdata('pendidikan'); ?>
            <div class="card-header">Data Pendidikan</div>
            <div class="card-body">
            <div class="row">
                <div class="col-4">
                    <h5>Jenjang</h5>
                    <h5>Nama Sekolah / Universitas</h5>
                    <h5>Fakultas</h5>
                    <h5>Jurusan</h5>
                    <h5>Tahun Masuk</h5>
                    <h5>Tahun Lulus</h5>
                </div>
                <div class="col-1">
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                    <h5>:</h5>
                </div>
                <div class="col-7">
                    <h5><?= $query[0]['jenjang'] ?></h5>
                    <h5><?= $query[0]['nama_sekolah_universitas'] ?></h5>
                    <h5><?= $query[0]['fakultas'] ?></h5>
                    <h5><?= $query[0]['jurusan'] ?></h5>
                    <h5><?= $query[0]['tahun_masuk'] ?></h5>
                    <h5><?= $query[0]['tahun_lulus'] ?></h5>
                </div>
            </div>
            <?php if($role != "super_admin"){ ?>
            <div class="row">
                <div class="col-12 text-center mt-4">
                    <a href="<?= base_url("karyawan/edit_data_pendidikan/").base64_encode($query[0]['nik']) ?>" class="btn btn-primary">Edit Data</a>
                </div>
            </div>
            <?php } ?>
            
            </div>
        </div>
    </div>
</main>