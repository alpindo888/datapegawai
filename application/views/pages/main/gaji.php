<style>
    .pdf{
        position: absolute;
        top: 7px;
        right: 20px;
        z-index: 2;
    }
    .excel{
        position: absolute;
        top: 199px;
        right: 230px;
        z-index: 2;
    }
</style>
<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="activity"></i></div>
                    <span>Data Gaji Karyawan</span>
                </h1>
                <div class="page-header-subtitle">Data Gaji Karyawan</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <a href="<?= base_url("laporan/gaji") ?>" class="btn btn-success excel">Download Excel</a>
        <div class="dropdown">
            <button class="btn btn-success pdf dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Download PDF
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="<?= base_url('penggajian/semuagaji') ?>">All</a>
                <?php foreach($periode as $value): ?>
                    <a class="dropdown-item" href="<?= base_url('penggajian/gaji_pdf/'.$value) ?>"><?= $value ?></a>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="card mb-4">
        <?= $this->session->flashdata('success'); ?>
        <?= $this->session->flashdata('delete'); ?>
            <div class="card-header">Data Gaji</div>
            <div class="card-body">
            <?php if($role != "super_admin"): ?>
                <?php if($role != 'user'): ?>
                <a href="<?= base_url("penggajian/tambah") ?>" class="btn btn-success mb-3">Tambah Data</a>
                <?php endif; ?>
            <?php endif; ?>
                <div class="datatable table-responsive" style="overflow-x:scroll;">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>NIK</th>
                                <th>Periode</th>
                                <th>Total Gaji</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>NIK</th>
                                <th>Periode</th>
                                <th>Total Gaji</th>
                                <th>Aksi</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php $no = 1; 
                            foreach($pegawai as $value): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= $value['nama'] ?></td>
                                    <td><?= $value['nik'] ?></td>
                                    <td><?= $value['periode'] ?></td>
                                    <td><?= rupiah($value['total']) ?></td>
                                    <td>
                                        <a href="<?= base_url(); ?>penggajian/view/<?= base64_encode($value['nik']) ?>" class="btn btn-datatable btn-icon btn-transparent-dark mr-2"><i class="text-primary" data-feather="eye"></i></a>
                                        <a href="<?= base_url(); ?>penggajian/delete/<?= base64_encode($value['id_gaji']) ?>" onclick="return confirm('Apakah anda yakin ingin menghapus data ? ')" class="btn btn-datatable btn-icon btn-transparent-dark"><i class="text-danger" data-feather="trash-2"></i></a>
                                        <a href="<?= base_url(); ?>penggajian/view_detail_pdf/<?= base64_encode($value['nik']) ?>" class="btn btn-datatable btn-icon btn-transparent-dark ml-2"><i class="text-success" data-feather="download"></i></a>
                                    </td>
                                </tr>
                            <?php $no++; 
                            endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>