<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="activity"></i></div>
                    <span>Edit Data Riwayat Pekerjaan</span>
                </h1>
                <div class="page-header-subtitle">Mendedit Data Riwayat Pekerjaan</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header">Edit Data Riwayat Pekerjaan</div>
                    <div class="card-body">
                        <div class="datatable table-responsive">
                            <form method="post">
                                <div class="row mt-5">
                                    <div class="col-12 text-center">
                                        <h3>Data Riwayat Pekerjaan</h3>
                                    </div>
                                </div>
                                <input type="hidden" name="id_pekerjaan" value="<?= $getdata['id_pekerjaan'] ?>">
                                <input type="hidden" name="nik" id="nik" value="<?= $getdata['nik'] ?>">
                                <div class="form-group">
                                    <label for="dari_bulan_tahun">Dari</label>
                                    <input type="date" class="form-control" name="dari_bulan_tahun" id="dari_bulan_tahun" value="<?= $getdata["dari_bulan_tahun"] ?>">
                                <?= form_error('dari_bulan_tahun', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="sampai_bulan_tahun">Sampai</label>
                                    <input type="date" class="form-control" name="sampai_bulan_tahun" id="sampai_bulan_tahun" value="<?= $getdata["sampai_bulan_tahun"] ?>">
                                <?= form_error('sampai_bulan_tahun', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="nama_perusahaan">Nama Perusahaan</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama_perusahaan" id="nama_perusahaan" placeholder="Masukan nama perusahaan" value="<?= $getdata["nama_perusahaan"] ?>">
                                <?= form_error('nama_perusahaan', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="alamat_perusahaan">Alamat Perusahaan</label>
                                    <input autocomplete="off" type="text" class="form-control" name="alamat_perusahaan" id="alamat_perusahaan" placeholder="Masukan alamat perusahaan" value="<?= $getdata["alamat_perusahaan"] ?>">
                                <?= form_error('alamat_perusahaan', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <a href="<?= base_url("karyawan/view/").base64_encode($getdata['nik']) ?>" class="btn btn-danger">Cancel</a>
                                <button class="btn btn-success">Edit Data</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

