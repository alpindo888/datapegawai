<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="activity"></i></div>
                    <span>Lihat Detail Gaji</span>
                </h1>
                <div class="page-header-subtitle">Lihat Detail Pegawai Gaji.</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="card mb-4">
        <?= $this->session->flashdata('gaji'); ?>
            <div class="card-header">Data Gaji</div>
            <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-3">
                            <h5>Nama</h5>
                        </div>
                        <div class="col-1">
                            <h5>:</h5>
                        </div>
                        <div class="col-8">
                            <h5><?= $query[0]['nama'] ?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-3">
                            <h5>NIK</h5>
                        </div>
                        <div class="col-1">
                            <h5>:</h5>
                        </div>
                        <div class="col-8">
                            <h5><?= $query[0]['nik'] ?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-3">
                            <h5>Periode</h5>
                        </div>
                        <div class="col-1">
                            <h5>:</h5>
                        </div>
                        <div class="col-8">
                            <h5><?= $query[0]['periode'] ?> </h5>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row mt-3">
                <div class="my-3"></div>
                <div class="col-md-6 col-12">
                    <div class="row">
                        <div class="col-6">
                            <h5>Gaji Pokok</h5>
                            <h5>Struktural</h5>
                            <h5>Fungsional</h5>
                            <h5>Utilities</h5>
                            <h5>Jumlah Hari Masuk</h5>
                            <h5>Transport</h5>
                            <h5>Jumlah Transport</h5>
                            <h5>Subsidi BPJS</h5>
                            <h5>Inval</h5>
                            <h5>Subsidi SPP</h5>
                            <h5>Lain-lain</h5>
                        

                        </div>
                        <div class="col-1">
                            <h5>:</h5>
                            <h5>:</h5>
                            <h5>:</h5>
                            <h5>:</h5>
                            <h5>:</h5>
                            <h5>:</h5>
                            <h5>:</h5>
                            <h5>:</h5>
                            <h5>:</h5>
                            <h5>:</h5>
                            <h5>:</h5>
                        </div>
                        <div class="col-5">
                            <h5><?= rupiah($query[0]['gaji_pokok']) ?></h5>
                            <h5><?= rupiah($query[0]['struktural']) ?></h5>
                            <h5><?= rupiah($query[0]['fungsional']) ?></h5>
                            <h5><?= rupiah($query[0]['utilities']) ?></h5>
                            <h5><?= rupiah($query[0]['jumlah_hari_masuk']) ?></h5>
                            <h5><?= rupiah($query[0]['transport']) ?></h5>
                            <h5><?= rupiah($query[0]['jumlah_transport']) ?></h5>
                            <h5><?= rupiah($query[0]['subsidi_bpjs']) ?></h5>
                            <h5><?= rupiah($query[0]['inval']) ?></h5>
                            <h5><?= rupiah($query[0]['subsidi_spp']) ?></h5>
                            <h5><?= rupiah($query[0]['lain_lain']) ?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="row">
                        <div class="col-6">
                            <h5>Potongan BPJS</h5>
                            <h5>Potongan Absensi</h5>
                            <h5>Potongan SPP Anak</h5>
                            <h5>Angsuran Sekolah</h5>
                            <h5>Pinjaman</h5>
                            <h5>Potongan PGRI</h5>
                        

                        </div>
                        <div class="col-1">
                            <h5>:</h5>
                            <h5>:</h5>
                            <h5>:</h5>
                            <h5>:</h5>
                            <h5>:</h5>
                            <h5>:</h5>
                        </div>
                        <div class="col-5">
                            <h5><?= rupiah($query[0]['potongan_bpjs']) ?></h5>
                            <h5><?= rupiah($query[0]['potongan_absensi']) ?></h5>
                            <h5><?= rupiah($query[0]['potongan_spp_anak']) ?></h5>
                            <h5><?= rupiah($query[0]['potongan_angsuran_sekolah']) ?></h5>
                            <h5><?= rupiah($query[0]['potongan_pinjaman']) ?></h5>
                            <h5><?= rupiah($query[0]['potongan_pgri']) ?></h5>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-6">
                            <h5>Total Penerimaan</h5>
                        </div>
                        <div class="col-1">
                            <h5>:</h5>
                        </div>
                        <div class="col-5">
                            <h5 class="text-primary"><?= rupiah($query[0]['total_penerimaan']) ?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-6">
                            <h5>Total Potongan</h5>
                        </div>
                        <div class="col-1">
                            <h5>:</h5>
                        </div>
                        <div class="col-5">
                            <h5 class="text-danger"><?= rupiah($query[0]['total_potongan']) ?></h5>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-3">
                    <h5>Total Bersih</h5>
                </div>
                <div class="col-1">
                    <h5>:</h5>
                </div>
                <div class="col-8">
                    <h5 class="text-success"><?= rupiah($query[0]['total']) ?></h5>
                </div>
            </div>

            <?php if($role != "super_admin"){ ?>
            <div class="row">
                <div class="col-12 text-center mt-4">
                    <a href="<?= base_url("penggajian/edit/").base64_encode($query[0]['nik']) ?>" class="btn btn-primary">Edit Data</a>
                </div>
            </div>
            <?php } ?>
            
            </div>
        </div>
</main>