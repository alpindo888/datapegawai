<style>
    .pdf{
        position: absolute;
        top: 199px;
        right: 42px;
        z-index: 2;
    }
    .excel{
        position: absolute;
        top: 199px;
        right: 200px;
        z-index: 2;
    }
</style>

<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="activity"></i></div>
                    <span>Data Pegawai</span>
                </h1>
                <div class="page-header-subtitle">Data Pegawai</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <a href="<?= base_url("laporan/pegawai") ?>" class="btn btn-success excel">Download Excel</a>
        <a href="<?= base_url("karyawan/all_karyawan_pdf") ?>" class="btn btn-success pdf">Download PDF</a>
        <div class="card mb-4">
        <?= $this->session->flashdata('success'); ?>
        <?= $this->session->flashdata('delete'); ?>
            <div class="card-header">Data Pegawai</div>
            <div class="card-body">
            <?php if($role != "super_admin"){  ?>
            <a href="<?= base_url("karyawan/tambah") ?>" class="btn btn-success mb-3">Tambah Data</a>
            <?php } ?>
                <div class="datatable table-responsive" style="overflow-x:scroll;">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Kode Pegawai</th>
                                <th>NIK</th>
                                <th>Status Kepegawaian</th>
                                <th>Images</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Kode Pegawai</th>
                                <th>NIK</th>
                                <th>Status Kepegawaian</th>
                                <th>Images</th>
                                <th>Aksi</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php $no = 1; 
                            foreach($pegawai as $value): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= $value['nama'] ?></td>
                                    <td><?= $value['kode_pegawai'] ?></td>
                                    <td><?= $value['nik'] ?></td>
                                    <td><?= $value['status_kepegawaian'] ?></td>
                                    <td><img src="<?= base_url('assets/img/img_user/').$value['image'] ?>" alt="" class="img-fluid" style="width:70px; height:70px;"></td>
                                    <td>
                                        <a href="<?= base_url(); ?>karyawan/view/<?= base64_encode($value['nik']) ?>" class="btn btn-datatable btn-icon btn-transparent-dark mr-2"><i class="text-primary" data-feather="eye"></i></a>
                                        <?php if($role != "super_admin"){ ?>
                                        <a href="<?= base_url(); ?>karyawan/delete_karyawan/<?= base64_encode($value['nik']) ?>" onclick="return confirm('Apakah anda yakin ingin menghapus data ? ')" class="btn btn-datatable btn-icon btn-transparent-dark mr-2"><i class="text-danger" data-feather="trash-2"></i></a>
                                        <?php } ?>
                                        <a href="<?= base_url(); ?>karyawan/detail_karyawan_pdf/<?= base64_encode($value['nik']) ?>" class="btn btn-datatable btn-icon btn-transparent-dark mr-2"><i class="text-success" data-feather="download"></i></a>
                                    </td>
                                </tr>
                            <?php $no++; 
                            endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>