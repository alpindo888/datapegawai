<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="activity"></i></div>
                    <span>Edit Data Keluarga</span>
                </h1>
                <div class="page-header-subtitle">Mengedit Data Keluarga</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header">Edit Data Keluarga</div>
                    <div class="card-body">
                        <div class="datatable table-responsive">
                            <form method="post">
                                <div class="row mt-5">
                                    <div class="col-12 text-center">
                                        <h3>Data Suami / Istri</h3>
                                    </div>
                                </div>
                                <input type="hidden" name="id_datakeluarga" value="<?= $getdata['id_datakeluarga'] ?>">
                                <input type="hidden" name="nik" id="nik" value="<?= $getdata['nik'] ?>">
                                <div class="form-group">
                                    <label for="nama_suami">Nama Suami / Istri</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama_suami" id="nama_suami" placeholder="Masukan nama suami / istri" value="<?= $getdata["nama_suami"] ?>" readonly>
                                <?= form_error('nama_suami', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tempat_lahir_suami">Tempat Lahir</label>
                                    <input autocomplete="off" type="text" class="form-control" name="tempat_lahir_suami" id="tempat_lahir_suami" placeholder="Masukan tempat lahir" value="<?= $getdata["tempat_lahir_suami"] ?>">
                                <?= form_error('tempat_lahir_suami', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_lahir_suami">Tanggal Lahir</label>
                                    <input type="date" class="form-control" name="tanggal_lahir_suami" id="tanggal_lahir_suami" value="<?= $getdata["tanggal_lahir_suami"] ?>">
                                <?= form_error('tanggal_lahir_suami', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="pekerjaan_suami">Pekerjaan</label>
                                    <input autocomplete="off" type="text" class="form-control" name="pekerjaan_suami" id="pekerjaan_suami" placeholder="Masukan pekerjaan" value="<?= $getdata["pekerjaan_suami"] ?>" readonly>
                                <?= form_error('pekerjaan_suami', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="row mt-5">
                                    <div class="col-12 text-center">
                                        <h3>Data Ibu</h3>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama_ibu">Nama Ibu</label>
                                    <input readonly autocomplete="off" type="text" class="form-control" name="nama_ibu" id="nama_ibu" placeholder="Masukan nama ibu" value="<?= $getdata["nama_ibu"] ?>">
                                <?= form_error('nama_ibu', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tempat_lahir_ibu">Tempat Lahir</label>
                                    <input autocomplete="off" type="text" class="form-control" name="tempat_lahir_ibu" id="tempat_lahir_ibu" placeholder="Masukan tempat lahir" value="<?= $getdata["tempat_lahir_ibu"] ?>">
                                <?= form_error('tempat_lahir_ibu', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_lahir_ibu">Tanggal Lahir</label>
                                    <input type="date" class="form-control" name="tanggal_lahir_ibu" id="tanggal_lahir_ibu" value="<?= $getdata["tanggal_lahir_ibu"] ?>">
                                <?= form_error('tanggal_lahir_ibu', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="pekerjaan_ibu">Pekerjaan Ibu</label>
                                    <input autocomplete="off" type="text" class="form-control" name="pekerjaan_ibu" id="pekerjaan_ibu" placeholder="Masukan pekerjaan ibu" value="<?= $getdata["pekerjaan_ibu"] ?>">
                                <?= form_error('pekerjaan_ibu', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="row mt-5">
                                    <div class="col-12 text-center">
                                        <h3>Data Ayah</h3>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama_ayah">Nama ayah</label>
                                    <input autocomplete="off" type="text" class="form-control" name="nama_ayah" id="nama_ayah" placeholder="Masukan nama ayah" value="<?= $getdata["nama_ayah"] ?>">
                                <?= form_error('nama_ayah', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tempat_lahir_ayah">Tempat Lahir</label>
                                    <input autocomplete="off" type="text" class="form-control" name="tempat_lahir_ayah" id="tempat_lahir_ayah" placeholder="Masukan tempat lahir" value="<?= $getdata["tempat_lahir_ayah"] ?>">
                                <?= form_error('tempat_lahir_ayah', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_lahir_ayah">Tanggal Lahir</label>
                                    <input type="date" class="form-control" name="tanggal_lahir_ayah" id="tanggal_lahir_ayah" value="<?= $getdata["tanggal_lahir_ayah"] ?>">
                                <?= form_error('tanggal_lahir_ayah', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="pekerjaan_ayah">Pekerjaan ayah</label>
                                    <input autocomplete="off" type="text" class="form-control" name="pekerjaan_ayah" id="pekerjaan_ayah" placeholder="Masukan pekerjaan ayah" value="<?= $getdata["pekerjaan_ayah"] ?>">
                                <?= form_error('pekerjaan_ayah', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <a href="<?= base_url("karyawan/view/").base64_encode($getdata['nik']) ?>" class="btn btn-danger">Cancel</a>
                                <button class="btn btn-success">Edit Data</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

