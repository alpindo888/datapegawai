<style type="text/css">
    .ui-datepicker-calendar {
        display: none;
    }
</style>

<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="activity"></i></div>
                    <span>Tambah Data Gaji</span>
                </h1>
                <div class="page-header-subtitle">Tambahkan Data Gaji</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header">Tambah Data Gaji</div>
                    <div class="card-body">
                        <div class="datatable table-responsive">
                            <form method="post" action="<?= base_url("penggajian/tambah") ?>">
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <select class="form-control" name="nama" id="nama">
                                        <?php foreach($pegawai as $value): ?>
                                        <option value="<?= $value['nama'] ?>"><?= $value['nama']  ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="nama">Periode</label>
                                        <input name="periode" id="startDate" class="form-control date-picker" placeholder="Enter Periode" autocomplete="off" />
                                </div>
                                <div class="row">
                                    <div class="col-xl-6 col-md-6 col-12 border">
                                        <h2 class="text-center mt-3">PENERIMAAN</h2>
                                        <hr>
                                        <div class="form-group">
                                            <label for="gaji_pokok">Gaji Pokok</label>
                                            <input type="number" class="form-control plus" name="gaji_pokok" id="gaji_pokok" placeholder="Enter gaji_pokok" value="0">
                                        <?= form_error('gaji_pokok', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="struktural">Struktural</label>
                                            <input type="number" class="form-control plus" name="struktural" id="struktural" placeholder="Enter struktural" value="0">
                                        <?= form_error('struktural', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="fungsional">Fungsional</label>
                                            <input type="number" class="form-control plus" name="fungsional" id="fungsional" placeholder="Enter fungsional" value="0">
                                        <?= form_error('fungsional', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="utilities">Utilities</label>
                                            <input type="number" class="form-control plus" name="utilities" id="utilities" placeholder="Enter utilities" value="0">
                                        <?= form_error('utilities', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="jumlah_hari_masuk">Jumlah Hari Masuk</label>
                                            <input type="number" class="form-control kali" name="jumlah_hari_masuk" id="jumlah_hari_masuk" oninput="format(this)" placeholder="Enter jumlah hari masuk" value="0" max="31">
                                        <?= form_error('jumlah_hari_masuk', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="transport">Transport</label>
                                            <input type="number" class="form-control kali" name="transport" id="transport" placeholder="Enter transport" value="0">
                                        <?= form_error('transport', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="jumlah_transport">Jumlah Transport</label>
                                            <input type="number" class="form-control plus" name="jumlah_transport" id="jumlah_transport" placeholder="Enter jumlah transport" value="0" readonly>
                                        <?= form_error('jumlah_transport', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="subsidi_bpjs">Subsidi BPJS</label>
                                            <input type="number" class="form-control plus" name="subsidi_bpjs" id="subsidi_bpjs" placeholder="Enter subsidi bpjs" value="0">
                                        <?= form_error('subsidi_bpjs', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="inval">Inval</label>
                                            <input type="number" class="form-control plus" name="inval" id="inval" placeholder="Enter inval" value="0">
                                        <?= form_error('inval', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="subsidi_spp">Subsidi SPP</label>
                                            <input type="number" class="form-control plus" name="subsidi_spp" id="subsidi_spp" placeholder="Enter subsidi spp" value="0">
                                        <?= form_error('subsidi_spp', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="lain_lain">Lain-lain</label>
                                            <input type="number" class="form-control plus" name="lain_lain" id="lain_lain" placeholder="Enter lain_lain" value="0">
                                        <?= form_error('lain_lain', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 col-12 border">
                                        <h2 class="text-center mt-3">POTONGAN</h2>
                                        <hr>
                                        <div class="form-group">
                                            <label for="potongan_bpjs">Potongan BPJS</label>
                                            <input type="number" class="form-control minus" name="potongan_bpjs" id="potongan_bpjs" placeholder="Enter potongan bpjs" value="0">
                                        <?= form_error('potongan_bpjs', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="potongan_absensi">Potongan Absensi</label>
                                            <input type="number" class="form-control minus" name="potongan_absensi" id="potongan_absensi" placeholder="Enter potongan absensi" value="0">
                                        <?= form_error('potongan_absensi', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="potongan_spp_anak">Potongan SPP Anak</label>
                                            <input type="number" class="form-control minus" name="potongan_spp_anak" id="potongan_spp_anak" placeholder="Enter potongan spp anak" value="0">
                                        <?= form_error('potongan_spp_anak', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="potongan_angsuran_sekolah">Angsuran Sekolah Anak</label>
                                            <input type="number" class="form-control minus" name="potongan_angsuran_sekolah" id="potongan_angsuran_sekolah" placeholder="Enter potongan angsuran sekolah" value="0">
                                        <?= form_error('potongan_angsuran_sekolah', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="potongan_pinjaman">Potongan Pinjaman</label>
                                            <input type="number" class="form-control minus" name="potongan_pinjaman" id="potongan_pinjaman" placeholder="Enter potongan_pinjaman" value="0">
                                        <?= form_error('potongan_pinjaman', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label for="potongan_pgri">Potongan PGRI</label>
                                            <input type="number" class="form-control minus" name="potongan_pgri" id="potongan_pgri" placeholder="Enter potongan pgri" value="0">
                                        <?= form_error('potongan_pgri', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-xl-6 col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="total_penerimaan"><b>TOTAL PENERIMAAN</b></label>
                                            <input type="number" class="form-control" name="total_penerimaan" id="total_penerimaan" readonly>
                                        <?= form_error('total_penerimaan', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="total_potongan"><b>TOTAL POTONGAN</b></label>
                                            <input type="number" class="form-control" name="total_potongan" id="total_potongan" readonly>
                                        <?= form_error('total_potongan', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="total"><b>TOTAL BERSIH</b></label>
                                            <input type="number" class="form-control" name="total" id="total" readonly>
                                        <?= form_error('total', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                    </div>
                                </div>
                                
                                
                        
                                <button class="btn btn-success">Tambah Data</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>

    $(document).ready(function() {

        $('.date-picker').datepicker({
            dateFormat: "mm-yy",
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            onClose: function(dateText, inst) {


                function isDonePressed(){
                    return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
                }

                if (isDonePressed()){
                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');
                    
                     $('.date-picker').focusout()//Added to remove focus from datepicker input box on selecting date
                }
            },
            beforeShow : function(input, inst) {

                inst.dpDiv.addClass('month_year_datepicker')

                if ((datestr = $(this).val()).length > 0) {
                    year = datestr.substring(datestr.length-4, datestr.length);
                    month = datestr.substring(0, 2);
                    $(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
                    $(this).datepicker('setDate', new Date(year, month-1, 1));
                    $(".ui-datepicker-calendar").hide();
                }
            }
        })
        

        //this calculates values automatically 
        calculate();
        calculatemin();
        kali();
        totalbersih();

        $(".plus").on("keydown keyup", function() {
            calculate();
            calculatemin();
            kali();
            totalbersih();
        });

        $(".minus").on("keydown keyup", function() {
            calculatemin();
            calculate();
            kali();
            totalbersih();
        });

        $(".kali").on("keyup", function(){
            kali();
            calculate();
            calculatemin();
            totalbersih();
        });

    });

    function kali()
    {
        var hrmsk =  $("#jumlah_hari_masuk").val();
        var trans = $("#transport").val();

        jml = hrmsk * trans;

        $("#jumlah_transport").val(jml);
    }

    function calculate()
    {
        var sum = 0;
        //iterate through each textboxes and add the values
        $(".plus").each(function() {
            //add only if the value is number
            if (!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);
            }
        });

        $("#total_penerimaan").val(sum);
    }

    function calculatemin()
    {
        var sum = 0;
        //iterate through each textboxes and add the values
        $(".minus").each(function() {
            //add only if the value is number
            if (!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);
            }
        });

        $("#total_potongan").val(sum);
    }

    function totalbersih()
    {
        
        var penerimaan = parseFloat(document.getElementById("total_penerimaan").value)
        var potongan = parseFloat(document.getElementById("total_potongan").value)

        total = document.getElementById("total");
        total.value = penerimaan - potongan;
        
    }
    function format(input){
        if(input.value < 0) input.value=Math.abs(input.value);
        if(input.value.length > 2) input.value = input.value.slice(0, 2);
        $(input).blur(function() {
        // if(input.value.length == 1) input.value=0+input.value;
        // if(input.value.length == 0) input.value='01';
        //* if you want to allow insert only 2 digits *//
        });
    }

</script>