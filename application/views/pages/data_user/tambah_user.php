<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="activity"></i></div>
                    <span>Tambah User</span>
                </h1>
                <div class="page-header-subtitle">Tambahkan user sesuai level</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="card bg-warning text-white mb-4">
                    <div class="card-body">Super Admin</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <h5 class="text-white"><i class="fa fa-user mr-3"></i><?= count($superadmin) ?></h5>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card bg-success text-white mb-4">
                    <div class="card-body">Admin</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <h5 class="text-white"><i class="fa fa-user mr-3"></i><?= count($admin) ?></h5>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card bg-danger text-white mb-4">
                <div class="card-body">User</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <h5 class="text-white"><i class="fa fa-user mr-3"></i><?= count($user) ?></h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header">Tambah Account</div>
                    <div class="card-body">
                        <div class="datatable table-responsive">
                            <form method="post" action="<?= base_url("user/tambah_user") ?>">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Enter Email" value="<?= set_value("email") ?>">
                                <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="password1">Password</label>
                                            <input type="password" class="form-control" name="password1" id="password1" placeholder="Enter Password">
                                            <?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="password2">Repeat Password</label>
                                            <input type="password" class="form-control" name="password2" id="password2" placeholder="Enter Password">
                                        </div>
                                    </div>
                                </div>
                                <?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
                                <input type="hidden" class="form-control" name="date_created" id="date_created" value="<?= date("d/m/Y") ?>">
                                <div class="form-group">
                                    <label for="Level">Level Account</label>
                                    <select class="form-control" name="level" id="Level">
                                        <option value="user">User</option>
                                        <option value="admin">Admin</option>
                                        <option value="super_admin">Super Admin</option>
                                    </select>
                                </div>
                        
                                <button class="btn btn-success">Tambah Data</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


