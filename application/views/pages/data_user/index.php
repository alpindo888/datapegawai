<style>
    .pdf{
        position: absolute;
        top: 355px;
        right: 35px;
        z-index: 2;
    }
</style>
<main>
    <div class="page-header pb-10 page-header-dark bg-gradient-primary-to-secondary">
        <div class="container-fluid">
            <div class="page-header-content">
                <h1 class="page-header-title">
                    <div class="page-header-icon"><i data-feather="activity"></i></div>
                    <span>Jumlah User</span>
                </h1>
                <div class="page-header-subtitle">Jumlah user berdasarkan level.</div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-n10">
        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="card bg-warning text-white mb-4">
                    <div class="card-body">Super Admin</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <h5 class="text-white"><i class="fa fa-user mr-3"></i><?= count($superadmin) ?></h5>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card bg-success text-white mb-4">
                    <div class="card-body">Admin</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <h5 class="text-white"><i class="fa fa-user mr-3"></i><?= count($admin) ?></h5>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card bg-danger text-white mb-4">
                <div class="card-body">User</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <h5 class="text-white"><i class="fa fa-user mr-3"></i><?= count($user) ?></h5>
                    </div>
                </div>
            </div>
        </div>
        <a href="<?= base_url("user/laporan") ?>" class="btn btn-success pdf">Download PDF</a>
        <div class="card mb-4">
        <?= $this->session->flashdata('success'); ?>
        <?= $this->session->flashdata('delete'); ?>
            <div class="card-header">Data Account</div>
            <div class="card-body">
            <?php if($role != "super_admin"){ ?>
            <a href="<?= base_url("user/tambah_user") ?>" class="btn btn-success mb-3">Tambah Data</a>
            <?php } ?>
                <div class="datatable table-responsive" style="overflow-x:scroll;">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Email</th>
                                <th>Level</th>
                                <th>Tanggal Pembuatan</th>
                                <?php if($role != "super_admin"){ ?>
                                <th>Action</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Email</th>
                                <th>Level</th>
                                <th>Tanggal Pembuatan</th>
                                <?php if($role != "super_admin"){ ?>
                                <th>Action</th>
                                <?php } ?>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php if(count($users) != 0): ?>
                                <?php $no=1;
                                    foreach($users as $key => $value): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= $value['email'] ?></td>
                                    <td><?= $value['level'] ?></td>
                                    <td><?= $value['date_created'] ?></td>
                                    <?php if($role != "super_admin"){ ?>
                                    <td>
                                        <a href="<?= base_url(); ?>user/edit_user/<?= $value['id_user'] ?>" class="btn btn-datatable btn-icon btn-transparent-dark mr-2"><i class="text-primary" data-feather="edit"></i></a>
                                        <a href="<?= base_url(); ?>user/delete_user/<?= $value['id_user'] ?>" onclick="return confirm('Are You Sure Deleting Account ? ')" class="btn btn-datatable btn-icon btn-transparent-dark"><i class="text-danger" data-feather="trash-2"></i></a>
                                    </td>
                                    <?php } ?>
                                </tr>
                                <?php $no++;
                                 endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>