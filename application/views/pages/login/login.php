<div class="container-login100" style="background-color: #4e73df;">
    <div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
        <span class="login100-form-title p-b-32">Account Login</span>
        <div class="col-12 px-0 pt-1 pb-2 text-center">
            <?php if($this->session->flashdata('msg')){
             echo '<span class="border-bottom">'.$this->session->flashdata('msg').'</span>';
            } ?>
        </div>   
        <form action="<?= base_url('login/check_auth'); ?>" method="post" class="login100-form validate-form flex-sb flex-w">
            <span class="txt1 p-b-11">Email</span>
            <div class="wrap-input100 validate-input m-b-36" data-validate = "Username is required">
                <input class="input100" type="email" name="email" >
                <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                <span class="focus-input100"></span>
            </div>
            <span class="txt1 p-b-11">Password</span>
            <div class="wrap-input100 validate-input m-b-12" data-validate = "Password is required">
                <span class="btn-show-pass">
                    <i class="fa fa-eye" onclick="showPass()"></i>
                </span>
                <input class="input100" type="password" name="password" id="pass">
                <?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
                <span class="focus-input100"></span>
            </div>
            <div class="container-login100-form-btn">
                <button type="submit" class="login100-form-btn">Login</button>
            </div>
        </form>
    </div>
</div>
<script>
    let eye = document.getElementById('pass')
    function showPass() {
        if(eye.type == "password") {
            eye.type = "text"
            setTimeout(() => {
                eye.type = "password"
            }, 2000);
        } else {
            eye.type = "password"
        }
    }
</script>