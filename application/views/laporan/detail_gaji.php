<style>
    .page_break { page-break-before: always; }
    .row:after {
        content: "";
        display: table;
        clear: both;
    }
    .column {
        float: left;
        width: 20%;
        padding: 0px;
    }
    .column2 {
        float: left;
        width: 10%;
        padding: 0px;
    }
    .column3{
        float: left;
        width: 70%;
        padding: 0px;
    }
    .column4{
        float: left;
        width: 70%;
        padding: 0px;
    }
    .column5{
        float:left;
        width:100%;
        padding:0px;
    }
</style>
<center><h1>Slip Gaji</h1></center>

<div class="row">
    <div class="column">
        <h5>Nama</h5>
        <h5>NIK</h5>
        <h5>Periode</h5>
        <hr>
    </div>
    <div class="column2">
        <h5>:</h5>
        <h5>:</h5>
        <h5>:</h5>
        <hr>
    </div>
    <div class="column3">
        <h5><?= $alldata[0]['nama'] ?></h5>
        <h5><?= $alldata[0]['nik'] ?></h5>
        <h5><?= $alldata[0]['periode'] ?></h5>
        <hr>
    </div>
</div>
<br>
<div class="row">
    <div class="column">
        <h5>Gaji Pokok</h5>
        <h5>Struktural</h5>
        <h5>Fungsional</h5>
        <h5>Utilities</h5>
        <h5>Jumlah Hari Masuk</h5>
        <h5>Transport</h5>
        <h5>Jumlah Transport</h5>
        <h5>Subsidi BPJS</h5>
        <h5>Inval</h5>
        <h5>Subsidi SPP</h5>
        <h5>Lain-lain</h5>
    </div>
    <div class="column2">
        <h5>:</h5>
        <h5>:</h5>
        <h5>:</h5>
        <h5>:</h5>
        <h5>:</h5>
        <h5>:</h5>
        <h5>:</h5>
        <h5>:</h5>
        <h5>:</h5>
        <h5>:</h5>
        <h5>:</h5>
    </div>
    <div class="column">
        <h5><?= rupiah($alldata[0]['gaji_pokok']) ?></h5>
        <h5><?= rupiah($alldata[0]['struktural']) ?></h5>
        <h5><?= rupiah($alldata[0]['fungsional']) ?></h5>
        <h5><?= rupiah($alldata[0]['utilities']) ?></h5>
        <h5><?= rupiah($alldata[0]['jumlah_hari_masuk']) ?></h5>
        <h5><?= rupiah($alldata[0]['transport']) ?></h5>
        <h5><?= rupiah($alldata[0]['jumlah_transport']) ?></h5>
        <h5><?= rupiah($alldata[0]['subsidi_bpjs']) ?></h5>
        <h5><?= rupiah($alldata[0]['inval']) ?></h5>
        <h5><?= rupiah($alldata[0]['subsidi_spp']) ?></h5>
        <h5><?= rupiah($alldata[0]['lain_lain']) ?></h5>
    </div>
    <div class="column">
        <h5>Potongan BPJS</h5>
        <h5>Potongan Absensi</h5>
        <h5>Potongan SPP Anak</h5>
        <h5>Angsuran Sekolah</h5>
        <h5>Pinjaman</h5>
        <h5>Potongan PGRI</h5>
    </div>
    <div class="column2">
        <h5>:</h5>
        <h5>:</h5>
        <h5>:</h5>
        <h5>:</h5>
        <h5>:</h5>
        <h5>:</h5>
    </div>
    <div class="column4">
        <h5><?= rupiah($alldata[0]['potongan_bpjs']) ?></h5>
        <h5><?= rupiah($alldata[0]['potongan_absensi']) ?></h5>
        <h5><?= rupiah($alldata[0]['potongan_spp_anak']) ?></h5>
        <h5><?= rupiah($alldata[0]['potongan_angsuran_sekolah']) ?></h5>
        <h5><?= rupiah($alldata[0]['potongan_pinjaman']) ?></h5>
        <h5><?= rupiah($alldata[0]['potongan_pgri']) ?></h5>
    </div>
</div>
<div class="row">
    <div class="column5">
        <hr>
    </div>
</div>
<div class="row">
    <div class="column">
        <h5>Total Penerimaan</h5>
    </div>
    <div class="column2">
        <h5>:</h5>
    </div>
    <div class="column">
        <h5 style="color:blue;"><?= rupiah($alldata[0]['total_penerimaan']) ?></h5>
    </div>
    <div class="column">
        <h5>Total Potongan</h5>
    </div>
    <div class="column2">
        <h5>:</h5>
    </div>
    <div class="column">
        <h5 style="color:red;"><?= rupiah($alldata[0]['total_potongan']) ?></h5>
    </div>
</div>
<div class="row">
    <div class="column5">
        <hr>
    </div>
</div>
<div class="row">
    <div class="column">
        <h5>Total Bersih</h5>
    </div>
    <div class="column2">
        <h5>:</h5>
    </div>
    <div class="column">
        <h5 style="color:green"><?= rupiah($alldata[0]['total']) ?></h5>
    </div>
</div>
<div class="row">
    <div class="column5">
        <hr>
    </div>
</div>





