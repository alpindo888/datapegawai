<style>
    table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
    }
</style>

<center><h1>Data Pegawai</h1></center>
<table style="width:100%">
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>NUPTK</th>
            <th>NIK</th>
            <th>Status Kepegawaian</th>
        </tr>
        <?php $no = 1; 
        foreach($pegawai as $value): ?>
            <tr>
                <td><?= $no ?></td>
                <td><?= $value['nama'] ?></td>
                <td><?= $value['nuptk'] ?></td>
                <td><?= $value['nik'] ?></td>
                <td><?= $value['status_kepegawaian'] ?></td>
                
            </tr>
        <?php $no++; 
        endforeach; ?>
</table>