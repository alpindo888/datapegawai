<style>
    .page_break { page-break-before: always; }
    .row:after {
        content: "";
        display: table;
        clear: both;
    }
    .column {
        float: left;
        width: 33.33%;
        padding: 10px;
        height: 300px; /* Should be removed. Only for demonstration */
    }
    .column2 {
        float: left;
        width: 10%;
        padding: 10px;
        height: 300px; /* Should be removed. Only for demonstration */
    }
</style>
<center><h1>Data Pegawai</h1></center>
<div class="row">
    <div class="column">
        <h3>Nama</h3>
        <h3>NUPTK</h3>
        <h3>NIK </h3>
        <h3>Jenis Kelamin</h3>
        <h3>Tempat Lahir</h3>
        <h3>Tanggal Lahir</h3>
        <h3>Status Kepegawaian</h3>
        <h3>Jabatan</h3>
        <h3>Agama</h3>
        <h3>Alamat</h3>
        <h3>NO HP</h3>
        <h3>Email</h3>
        <h3>Tmt</h3>
        <h3>Nama Ibu Kandung</h3>
        <h3>Status Perkawinan</h3>
        <h3>Nama Suami / Istri</h3>
        <h3>Pekerjaan Suami / Istri</h3>
        <h3>Jumlah Anak</h3>
        <h3>Jumlah Saudara</h3>
        <h3>NPWP</h3>
        <h3>Nama Wajib Pajak</h3>
        <h3>Kewarganegaraan</h3>
    </div>
    <div class="column2">
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
    </div>
    <div class="column">
        <h3><?= $alldata[0]['nama'] ?></h3>
        <h3><?= $alldata[0]['nuptk'] ?></h3>
        <h3><?= $alldata[0]['nik'] ?></h3>
        <h3><?= $alldata[0]['jenis_kelamin'] ?></h3>
        <h3><?= $alldata[0]['tempat_lahir'] ?></h3>
        <h3><?= date("d-m-Y", strtotime($alldata[0]['tanggal_lahir'])) ?></h3>
        <h3><?= $alldata[0]['status_kepegawaian'] ?></h3>
        <h3><?= $alldata[0]['jabatan'] ?></h3>
        <h3><?= $alldata[0]['agama'] ?></h3>
        <h3><?= $alldata[0]['alamat'] ?></h3>
        <h3><?= $alldata[0]['no_hp'] ?></h3>
        <h3><?= $alldata[0]['email'] ?></h3>
        <h3><?= $alldata[0]['tmt'] ?></h3>
        <h3><?= $alldata[0]['nama_ibu_kandung'] ?></h3>
        <h3><?= $alldata[0]['status_perkawinan'] ?></h3>
        <h3><?= $alldata[0]['nama_suami_istri'] ?></h3>
        <h3><?= $alldata[0]['pekerjaan_suami_istri'] ?></h3>
        <h3><?= $alldata[0]['jumlah_anak'] ?></h3>
        <h3><?= $alldata[0]['jumlah_saudara'] ?></h3>
        <h3><?= $alldata[0]['npwp'] ?></h3>
        <h3><?= $alldata[0]['nama_wajib_pajak'] ?></h3>
        <h3><?= $alldata[0]['kewarganegaraan'] ?></h3>
    </div>
</div>

<div class="page_break"></div>

<center><h1>Data Keluarga</h1></center>
<div class="row">
    <div class="column">
        <h3>Nama Suami / Istri</h3>
        <h3>Tempat Lahir Suami / Istri</h3>
        <h3>Tanggal Lahir Suami / Istri </h3>
        <h3>Pekerjaan Suami / Istri</h3>
        <h3>Nama Ibu</h3>
        <h3>Tempat Lahir Ibu</h3>
        <h3>Tanggal Lahir Ibu</h3>
        <h3>Pekerjaan Ibu</h3>
        <h3>Nama Ayah</h3>
        <h3>Tempat Lahir Ayah</h3>
        <h3>Tanggal Lahir Ayah</h3>
        <h3>Pekerjaan Ayah</h3>
    </div>
    <div class="column2">
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
    </div>
    <div class="column">
        <h3><?= $alldata[0]['nama_suami'] ?></h3>
        <h3><?= $alldata[0]['tempat_lahir_suami'] ?></h3>
        <h3><?= date("d-m-Y", strtotime($alldata[0]['tanggal_lahir_suami'])) ?></h3>
        <h3><?= $alldata[0]['pekerjaan_suami'] ?></h3>
        <h3><?= $alldata[0]['nama_ibu'] ?></h3>
        <h3><?= $alldata[0]['tempat_lahir_ibu'] ?></h3>
        <h3><?= date("d-m-Y", strtotime($alldata[0]['tanggal_lahir_ibu'])) ?></h3>
        <h3><?= $alldata[0]['pekerjaan_ibu'] ?></h3>
        <h3><?= $alldata[0]['nama_ayah'] ?></h3>
        <h3><?= $alldata[0]['tempat_lahir_ayah'] ?></h3>
        <h3><?= date("d-m-Y", strtotime($alldata[0]['tanggal_lahir_ayah'])) ?></h3>
        <h3><?= $alldata[0]['pekerjaan_ayah'] ?></h3>
    </div>
</div>
<div class="page_break"></div>
<center><h1>Data Riwayat Pekerjaan</h1></center>
<div class="row">
    <div class="column">
        <h3>Dari</h3>
        <h3>Sampai</h3>
        <h3>Nama Perusahaan</h3>
        <h3>Alamat Perusahaan</h3>
    </div>
    <div class="column2">
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
    </div>
    <div class="column">
        <h3><?= date("d-m-Y", strtotime($alldata[0]['dari_bulan_tahun'])) ?></h3>
        <h3><?= date("d-m-Y", strtotime($alldata[0]['sampai_bulan_tahun'])) ?></h3>
        <h3><?= $alldata[0]['nama_perusahaan'] ?></h3>
        <h3><?= $alldata[0]['alamat_perusahaan'] ?></h3>
    </div>
</div>
<div class="page_break"></div>
<center><h1>Data Pendidikan</h1></center>
<div class="row">
    <div class="column">
        <h3>Jenjang</h3>
        <h3>Nama Sekolah / Universitas</h3>
        <h3>Fakultas</h3>
        <h3>Jurusan</h3>
        <h3>Tahun Masuk</h3>
        <h3>Tahun Lulus</h3>
    </div>
    <div class="column2">
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
        <h3>:</h3>
    </div>
    <div class="column">
        <h3><?= $alldata[0]['jenjang'] ?></h3>
        <h3><?= $alldata[0]['nama_sekolah_universitas'] ?></h3>
        <h3><?= $alldata[0]['fakultas'] ?></h3>
        <h3><?= $alldata[0]['jurusan'] ?></h3>
        <h3><?= $alldata[0]['tahun_masuk'] ?></h3>
        <h3><?= $alldata[0]['tahun_lulus'] ?></h3>
    </div>
</div>