<style>
    table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
    }
</style>

<center><h1>Laporan Penggajian</h1></center>
<table style="width:100%">
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>NIK</th>
            <th>Periode</th>
            <th>Total</th>
        </tr>
        <?php  $no = 1; 
            foreach($pegawai as $value): ?>
                <tr>
                    <td><?= $no ?></td>
                    <td><?= $value['nama'] ?></td>
                    <td><?= $value['nik'] ?></td>
                    <td><?= $value['periode'] ?></td>
                    <td><?= rupiah($value['total']) ?></td>
                </tr>
            <?php $no++; 
            endforeach; ?>
</table>