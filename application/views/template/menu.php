<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <nav class="sidenav shadow-right sidenav-light">
            <div class="sidenav-menu">
                <div class="nav accordion" id="accordionSidenav">
                    <div class="sidenav-menu-heading">DATA PEGAWAI</div>
                    <a class="nav-link" href="<?= base_url('karyawan') ?>">
                        <div class="nav-link-icon"><i class="fas fa-users"></i></div>
                        Data Pegawai
                    </a>
                    <div class="sidenav-menu-heading">Kelola Gaji</div>
                    <a class="nav-link" href="<?= base_url('penggajian') ?>">
                        <div class="nav-link-icon"><i class="fas fa-money-check-alt"></i></div>
                        Data Penggajian
                    </a>
                    <?php if($this->session->role != 'user'){ ?>
                    <div class="sidenav-menu-heading">DATA ACCOUNT</div>
                        <a class="nav-link" href="<?= base_url('user') ?>">
                            <div class="nav-link-icon"><i data-feather="user"></i></div>
                            User
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="sidenav-footer">
                <div class="sidenav-footer-content">
                    <div class="sidenav-footer-subtitle">Logged in as: <br/><?= $this->session->user['email'] ?></div>
                    <div class="sidenav-footer-title"></div>
                </div>
            </div>
        </nav>
    </div>
    <div id="layoutSidenav_content">