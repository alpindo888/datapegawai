<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
    private $role;

    function __construct() {
		parent::__construct();
		$this->load->model("laporan_model");
		$this->load->library('session');
		$this->role = $this->session->userdata("role");

		if(!$this->role){
            redirect("login");
        }
	}
    
	public function pegawai()
	{
        $pribadi = $this->laporan_model->pribadi();
        $keluarga = $this->laporan_model->keluarga();
        $pendidikan = $this->laporan_model->pendidikan();
        $pekerjaan = $this->laporan_model->riwayatpekerjaan();

        require_once APPPATH.'PHPExcel-1.8/Classes/PHPExcel.php';
        require_once APPPATH.'PHPExcel-1.8/Classes/PHPExcel/IOFactory.php';

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Ricky")
            ->setLastModifiedBy("Ricky")
            ->setTitle("Reports")
            ->setSubject("Laporan")
            ->setDescription("Laporan Data Pegawai")
            ->setKeywords("phpExcel")
            ->setCategory("Laporan");
            
        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:V1');  
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'DATA PRIBADI');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Nama');
        $objPHPExcel->getActiveSheet()->setCellValue('B2', 'NUPTK');
        $objPHPExcel->getActiveSheet()->setCellValue('C2', 'NIK');
        $objPHPExcel->getActiveSheet()->setCellValue('D2', 'Jenis Kelamin');
        $objPHPExcel->getActiveSheet()->setCellValue('E2', 'Tempat Lahir');
        $objPHPExcel->getActiveSheet()->setCellValue('F2', 'Tanggal Lahir');
        $objPHPExcel->getActiveSheet()->setCellValue('G2', 'Status Kepegawaian');
        $objPHPExcel->getActiveSheet()->setCellValue('H2', 'Jabatan');
        $objPHPExcel->getActiveSheet()->setCellValue('I2', 'Agama');
        $objPHPExcel->getActiveSheet()->setCellValue('J2', 'Alamat');
        $objPHPExcel->getActiveSheet()->setCellValue('K2', 'No Hp');
        $objPHPExcel->getActiveSheet()->setCellValue('L2', 'Email');
        $objPHPExcel->getActiveSheet()->setCellValue('M2', 'TMT');
        $objPHPExcel->getActiveSheet()->setCellValue('N2', 'Nama Ibu Kandung');
        $objPHPExcel->getActiveSheet()->setCellValue('O2', 'Status Perkawinan');
        $objPHPExcel->getActiveSheet()->setCellValue('P2', 'Nama Suami / Istri');
        $objPHPExcel->getActiveSheet()->setCellValue('Q2', 'Pekerjaan Suami / Istri');
        $objPHPExcel->getActiveSheet()->setCellValue('R2', 'Jumlah Anak');
        $objPHPExcel->getActiveSheet()->setCellValue('S2', 'Jumlah Saudara');
        $objPHPExcel->getActiveSheet()->setCellValue('T2', 'NPWP');
        $objPHPExcel->getActiveSheet()->setCellValue('U2', 'Nama Wajib Pajak');
        $objPHPExcel->getActiveSheet()->setCellValue('V2', 'Kewarganegaraan');

        $n=3;

        foreach($pribadi as $value){
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$n, $value->nama);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$n, $value->nuptk);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$n, $value->nik);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$n, $value->jenis_kelamin);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$n, $value->tempat_lahir);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$n, $value->tanggal_lahir);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$n, $value->status_kepegawaian);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$n, $value->jabatan);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$n, $value->agama);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$n, $value->alamat);
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$n, $value->no_hp);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$n, $value->email);
            $objPHPExcel->getActiveSheet()->setCellValue('M'.$n, $value->tmt);
            $objPHPExcel->getActiveSheet()->setCellValue('N'.$n, $value->nama_ibu_kandung);
            $objPHPExcel->getActiveSheet()->setCellValue('O'.$n, $value->status_perkawinan);
            $objPHPExcel->getActiveSheet()->setCellValue('P'.$n, $value->nama_suami_istri);
            $objPHPExcel->getActiveSheet()->setCellValue('Q'.$n, $value->pekerjaan_suami_istri);
            $objPHPExcel->getActiveSheet()->setCellValue('R'.$n, $value->jumlah_anak);
            $objPHPExcel->getActiveSheet()->setCellValue('S'.$n, $value->jumlah_saudara);
            $objPHPExcel->getActiveSheet()->setCellValue('T'.$n, $value->npwp);
            $objPHPExcel->getActiveSheet()->setCellValue('U'.$n, $value->nama_wajib_pajak);
            $objPHPExcel->getActiveSheet()->setCellValue('V'.$n, $value->kewarganegaraan);
            
            $n++;
        }
        
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
    
        $objPHPExcel->getActiveSheet()->getStyle("A1:V3")->applyFromArray($style);
                    
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Data Pribadi');

    /////////////////////////////////////////////////////////////////////////////////////////////

        // Create a new worksheet, after the default sheet
        $objPHPExcel->createSheet();

        // Add some data to the second sheet, resembling some different data types
        $objPHPExcel->setActiveSheetIndex(1)->mergeCells('A1:M1');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'DATA KELUARGA');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'NIK');
        $objPHPExcel->getActiveSheet()->setCellValue('B2', 'Nama Suami / Istri');
        $objPHPExcel->getActiveSheet()->setCellValue('C2', 'Tempat Lahir Suami / Istri');
        $objPHPExcel->getActiveSheet()->setCellValue('D2', 'Tanggal Lahir Suami / Istri');
        $objPHPExcel->getActiveSheet()->setCellValue('E2', 'Pekerjaan Suami / Istri');
        $objPHPExcel->getActiveSheet()->setCellValue('F2', 'Nama Ibu');
        $objPHPExcel->getActiveSheet()->setCellValue('G2', 'Tempat Lahir Ibu');
        $objPHPExcel->getActiveSheet()->setCellValue('H2', 'Tanggal Lahir Ibu');
        $objPHPExcel->getActiveSheet()->setCellValue('I2', 'Pekerjaan Ibu');
        $objPHPExcel->getActiveSheet()->setCellValue('J2', 'Nama Ayah');
        $objPHPExcel->getActiveSheet()->setCellValue('K2', 'Tempat Lahir Ayah');
        $objPHPExcel->getActiveSheet()->setCellValue('L2', 'Tanggal Lahir Ayah');
        $objPHPExcel->getActiveSheet()->setCellValue('M2', 'Pekerjaan Ayah');

        $n=3;
        foreach($keluarga as $value){
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$n, $value->nik);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$n, $value->nama_suami);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$n, $value->tempat_lahir_suami);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$n, $value->tanggal_lahir_suami);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$n, $value->pekerjaan_suami);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$n, $value->nama_ibu);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$n, $value->tempat_lahir_ibu);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$n, $value->tanggal_lahir_ibu);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$n, $value->pekerjaan_ibu);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$n, $value->nama_ayah);
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$n, $value->tempat_lahir_ayah);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$n, $value->tanggal_lahir_ayah);
            $objPHPExcel->getActiveSheet()->setCellValue('M'.$n, $value->pekerjaan_ayah);
            
            $n++;
        }
        
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
    
        $objPHPExcel->getActiveSheet()->getStyle("A1:M3")->applyFromArray($style);

        // Rename 2nd sheet
        $objPHPExcel->getActiveSheet()->setTitle('Data Keluarga');
        /////////////////////////////////////////////////////////////////////////////////////////////

        $objPHPExcel->createSheet();

        // Add some data to the second sheet, resembling some different data types
        $objPHPExcel->setActiveSheetIndex(2)->mergeCells('A1:G1');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'DATA RIWAYAT PENDIDIKAN');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'NIK');
        $objPHPExcel->getActiveSheet()->setCellValue('B2', 'Jenjang');
        $objPHPExcel->getActiveSheet()->setCellValue('C2', 'Nama Sekolah / Universitas');
        $objPHPExcel->getActiveSheet()->setCellValue('D2', 'Fakultas');
        $objPHPExcel->getActiveSheet()->setCellValue('E2', 'Jurusan');
        $objPHPExcel->getActiveSheet()->setCellValue('F2', 'Tahun Masuk');
        $objPHPExcel->getActiveSheet()->setCellValue('G2', 'Tahun Lulus');

        $n=3;
        foreach($pendidikan as $value){
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$n, $value->nik);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$n, $value->jenjang);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$n, $value->nama_sekolah_universitas);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$n, $value->fakultas);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$n, $value->jurusan);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$n, $value->tahun_masuk);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$n, $value->tahun_lulus);
            
            $n++;
        }
        
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
    
        $objPHPExcel->getActiveSheet()->getStyle("A1:G3")->applyFromArray($style);

        // Rename 2nd sheet
        $objPHPExcel->getActiveSheet()->setTitle('Data Riwayat Pendidikan');

        /////////////////////////////////////////////////////////////////////////
        $objPHPExcel->createSheet();

        // Add some data to the second sheet, resembling some different data types
        $objPHPExcel->setActiveSheetIndex(3)->mergeCells('A1:E1');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'DATA RIWAYAT PEKERJAAN');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'NIK');
        $objPHPExcel->getActiveSheet()->setCellValue('B2', 'Dari');
        $objPHPExcel->getActiveSheet()->setCellValue('C2', 'Sampai');
        $objPHPExcel->getActiveSheet()->setCellValue('D2', 'Nama Perusahaan');
        $objPHPExcel->getActiveSheet()->setCellValue('E2', 'Alamat Perusahaan');

        $n=3;
        foreach($pekerjaan as $value){
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$n, $value->nik);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$n, $value->dari_bulan_tahun);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$n, $value->sampai_bulan_tahun);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$n, $value->nama_perusahaan);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$n, $value->alamat_perusahaan);
            
            $n++;
        }
        
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
    
        $objPHPExcel->getActiveSheet()->getStyle("A1:E3")->applyFromArray($style);

        // Rename 2nd sheet
        $objPHPExcel->getActiveSheet()->setTitle('Data Riwayat Pekerjaan');

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan Pegawai.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public function gaji()
    {
        $gaji = $this->laporan_model->gaji();

        require_once APPPATH.'PHPExcel-1.8/Classes/PHPExcel.php';
        require_once APPPATH.'PHPExcel-1.8/Classes/PHPExcel/IOFactory.php';

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Ricky")
            ->setLastModifiedBy("Ricky")
            ->setTitle("Reports")
            ->setSubject("Laporan")
            ->setDescription("Laporan Data Pegawai")
            ->setKeywords("phpExcel")
            ->setCategory("Laporan");
            
        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:V1');  
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'DATA GAJI');
        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'NIK');
        $objPHPExcel->getActiveSheet()->setCellValue('B2', 'Gaji Pokok');
        $objPHPExcel->getActiveSheet()->setCellValue('C2', 'Struktural');
        $objPHPExcel->getActiveSheet()->setCellValue('D2', 'Fungsional');
        $objPHPExcel->getActiveSheet()->setCellValue('E2', 'Utilities');
        $objPHPExcel->getActiveSheet()->setCellValue('F2', 'Jumlah Hari Masuk');
        $objPHPExcel->getActiveSheet()->setCellValue('G2', 'Transport');
        $objPHPExcel->getActiveSheet()->setCellValue('H2', 'Jumlah Transport');
        $objPHPExcel->getActiveSheet()->setCellValue('I2', 'Subsidi BPJS');
        $objPHPExcel->getActiveSheet()->setCellValue('J2', 'Inval');
        $objPHPExcel->getActiveSheet()->setCellValue('K2', 'Subsidi SPP');
        $objPHPExcel->getActiveSheet()->setCellValue('L2', 'Lain - Lain');
        $objPHPExcel->getActiveSheet()->setCellValue('M2', 'Potongan BPJS');
        $objPHPExcel->getActiveSheet()->setCellValue('N2', 'Potongan Absensi');
        $objPHPExcel->getActiveSheet()->setCellValue('O2', 'Potongan SPP Anak');
        $objPHPExcel->getActiveSheet()->setCellValue('P2', 'Potongan Angsuran Sekolah');
        $objPHPExcel->getActiveSheet()->setCellValue('Q2', 'Potongan Pinjaman');
        $objPHPExcel->getActiveSheet()->setCellValue('R2', 'Potongan PGRI');
        $objPHPExcel->getActiveSheet()->setCellValue('S2', 'Total Penerimaan');
        $objPHPExcel->getActiveSheet()->setCellValue('T2', 'Total Potongan');
        $objPHPExcel->getActiveSheet()->setCellValue('U2', 'Total');
        $objPHPExcel->getActiveSheet()->setCellValue('V2', 'Periode');

        $n=3;

        foreach($gaji as $value){
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$n, $value->nik);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$n, $value->gaji_pokok);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$n, $value->struktural);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$n, $value->fungsional);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$n, $value->utilities);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$n, $value->jumlah_hari_masuk);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$n, $value->transport);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$n, $value->jumlah_transport);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$n, $value->subsidi_bpjs);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$n, $value->inval);
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$n, $value->subsidi_spp);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$n, $value->lain_lain);
            $objPHPExcel->getActiveSheet()->setCellValue('M'.$n, $value->potongan_bpjs);
            $objPHPExcel->getActiveSheet()->setCellValue('N'.$n, $value->potongan_absensi);
            $objPHPExcel->getActiveSheet()->setCellValue('O'.$n, $value->potongan_spp_anak);
            $objPHPExcel->getActiveSheet()->setCellValue('P'.$n, $value->potongan_angsuran_sekolah);
            $objPHPExcel->getActiveSheet()->setCellValue('Q'.$n, $value->potongan_pinjaman);
            $objPHPExcel->getActiveSheet()->setCellValue('R'.$n, $value->potongan_pgri);
            $objPHPExcel->getActiveSheet()->setCellValue('S'.$n, $value->total_penerimaan);
            $objPHPExcel->getActiveSheet()->setCellValue('T'.$n, $value->total_potongan);
            $objPHPExcel->getActiveSheet()->setCellValue('U'.$n, $value->total);
            $objPHPExcel->getActiveSheet()->setCellValue('V'.$n, $value->periode);
            
            $n++;
        }
        
        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
    
        $objPHPExcel->getActiveSheet()->getStyle("A1:U3")->applyFromArray($style);
                    
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Data Penggajian');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan Gaji.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    
}
