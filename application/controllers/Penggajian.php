<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penggajian extends CI_Controller {
    private $role;


	function __construct() {
		parent::__construct();
        $this->load->library('session');
        $this->load->helper('pegawai');
        $this->load->model("gaji_model");
        $this->load->model("pegawai_model");

        $this->role = $this->session->userdata("role");
	}

	public function index()
	{
        
        if(!$this->role){
        
            redirect("login");
        
        }
        if ( $this->role == "user" ){

            $identity = $this->session->userdata("user")['email'];
            $datapegawai = $this->gaji_model->dataPegawai();
            $array = [];

            foreach($datapegawai as $value){
                if($value['email'] == $identity){
                    array_push($array, $value);
                }
            }

            $data['pegawai'] = $array;
        
        } else {

            $data['pegawai'] = $this->gaji_model->dataPegawai();
        
        }

        $periode = [];

        foreach($data['pegawai'] as $value){
            array_push($periode, $value['periode']);
        }

        $data['periode'] = array_unique($periode);

        function sortFunction( $a, $b ) {
            if ($a==$b) return 0;
            return ($a<$b)?-1:1;
        }
        usort($data['periode'], "sortFunction");

        $data['role'] = $this->session->userdata("role");
        
        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('template/topMenu');
        $this->load->view('pages/main/gaji', $data);
        $this->load->view('template/footer');
    }
    
    public function tambah()
    {

        if(!$this->role){
        
            redirect("login");
        
        } else {

            if($this->role == "user"){
                
                redirect("penggajian/index");
            }
        }

        $data['pegawai'] = $this->pegawai_model->dataPegawai();

        $this->form_validation->set_rules('gaji_pokok', 'Gaji Pokok', 'required|numeric');
        $this->form_validation->set_rules('struktural', 'Struktural', 'required|numeric');
        $this->form_validation->set_rules('fungsional', 'Fungsional', 'required|numeric');
        $this->form_validation->set_rules('utilities', 'Utilities', 'required|numeric');
        $this->form_validation->set_rules('jumlah_hari_masuk', 'Jumlah Hari Masuk', 'required|numeric');
        $this->form_validation->set_rules('transport', 'Transport', 'required|numeric');
        $this->form_validation->set_rules('jumlah_transport', 'Jumlah transport', 'required|numeric');
        $this->form_validation->set_rules('subsidi_bpjs', 'Subsidi BPJS', 'required|numeric');
        $this->form_validation->set_rules('inval', 'Inval', 'required|numeric');
        $this->form_validation->set_rules('subsidi_spp', 'Subsidi SPP', 'required|numeric');
        $this->form_validation->set_rules('lain_lain', 'Lain lain', 'required|numeric');
        $this->form_validation->set_rules('potongan_bpjs', 'Potongan BPJS', 'required|numeric');
        $this->form_validation->set_rules('potongan_absensi', 'Potongan Absensi', 'required|numeric');
        $this->form_validation->set_rules('potongan_spp_anak', 'Potongan SPP Anak', 'required|numeric');
        $this->form_validation->set_rules('potongan_angsuran_sekolah', 'Potongan Angsuran Sekolah', 'required|numeric');
        $this->form_validation->set_rules('potongan_pinjaman', 'Potongan Pinjaman', 'required|numeric');
        $this->form_validation->set_rules('potongan_pgri', 'Potongan PGRI', 'required|numeric');
        $this->form_validation->set_rules('total_penerimaan', 'Total Penerimaan', 'required|numeric');
        $this->form_validation->set_rules('total_potongan', 'Total Potongan', 'required|numeric');
        $this->form_validation->set_rules('total', 'Total Bersih', 'required|numeric');
        $this->form_validation->set_rules('periode', 'Periode', 'required');


        if ($this->form_validation->run() == false) {

			$this->load->view('template/header');
            $this->load->view('template/menu');
            $this->load->view('template/topMenu');
            $this->load->view('pages/main/tambah_data_gaji', $data);
            $this->load->view('template/footer');
        
        } else {

            $nama = $this->input->post("nama");
            $db = $this->db->get_where('tbl_pribadi', ['nama' => $nama])->row_array();
            $nik = $db['nik'];
    
            $data = [
                'nik' => htmlspecialchars($nik),
                'gaji_pokok' => htmlspecialchars($this->input->post('gaji_pokok')),
                'struktural' => htmlspecialchars($this->input->post('struktural')),
                'fungsional' => htmlspecialchars($this->input->post('fungsional')),
                'utilities' => htmlspecialchars($this->input->post('utilities')),
                'jumlah_hari_masuk' => htmlspecialchars($this->input->post('jumlah_hari_masuk')),
                'transport' => htmlspecialchars($this->input->post('transport')),
                'jumlah_transport' => htmlspecialchars($this->input->post('jumlah_transport')),
                'subsidi_bpjs' => htmlspecialchars($this->input->post('subsidi_bpjs')),
                'inval' => htmlspecialchars($this->input->post('inval')),
                'subsidi_spp' => htmlspecialchars($this->input->post('subsidi_spp')),
                'lain_lain' => htmlspecialchars($this->input->post('lain_lain')),
                'potongan_bpjs' => htmlspecialchars($this->input->post('potongan_bpjs')),
                'potongan_absensi' => htmlspecialchars($this->input->post('potongan_absensi')),
                'potongan_spp_anak' => htmlspecialchars($this->input->post('potongan_spp_anak')),
                'potongan_angsuran_sekolah' => htmlspecialchars($this->input->post('potongan_angsuran_sekolah')),
                'potongan_pinjaman' => htmlspecialchars($this->input->post('potongan_pinjaman')),
                'potongan_pgri' => htmlspecialchars($this->input->post('potongan_pgri')),
                'total_penerimaan' => htmlspecialchars($this->input->post('total_penerimaan')),
                'total_potongan' => htmlspecialchars($this->input->post('total_potongan')),
                'total' => htmlspecialchars($this->input->post('total')),
                'periode' => htmlspecialchars($this->input->post('periode'))
            ];
            
            $this->db->insert('tbl_gaji', $data);

			$this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Data Berhasil Di tambahkan !</div>');
            redirect('penggajian/index');
        
        }
        
    }

    public function delete($id)
    {
        if(!$this->role){
        
            redirect("login");
        
        } else {

            if($this->role == "user"){
                
                redirect("penggajian/index");
            }
        }

        $nik = base64_decode($id);

        $this->gaji_model->delete_gaji($nik);
        $this->session->set_flashdata('delete', '<div class="alert alert-danger" role="alert">Data Berhasil di hapus</div>');
        redirect('penggajian/index');
    }

    public function view($id)
    {
        if(!$this->role){
        
            redirect("login");
        
        }

        $nik = base64_decode($id);
        $data['role'] = $this->role;
        $data['query'] = $this->gaji_model->view_detail($nik);

        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('template/topMenu');
        $this->load->view('pages/main/view_gaji', $data);
        $this->load->view('template/footer');

    }

    public function edit($id)
    {
        if(!$this->role){
        
            redirect("login");
        
        } else {

            if($this->role == "user"){
                
                redirect("penggajian/index");
            }
        }

        $nik = base64_decode($id);
        $encode = base64_encode($nik);
        $data['getdata'] = $this->gaji_model->view_detail($nik);

        $this->form_validation->set_rules('gaji_pokok', 'Gaji Pokok', 'required|numeric');
        $this->form_validation->set_rules('struktural', 'Struktural', 'required|numeric');
        $this->form_validation->set_rules('fungsional', 'Fungsional', 'required|numeric');
        $this->form_validation->set_rules('utilities', 'Utilities', 'required|numeric');
        $this->form_validation->set_rules('jumlah_hari_masuk', 'Jumlah Hari Masuk', 'required|numeric');
        $this->form_validation->set_rules('transport', 'Transport', 'required|numeric');
        $this->form_validation->set_rules('jumlah_transport', 'Jumlah transport', 'required|numeric');
        $this->form_validation->set_rules('subsidi_bpjs', 'Subsidi BPJS', 'required|numeric');
        $this->form_validation->set_rules('inval', 'Inval', 'required|numeric');
        $this->form_validation->set_rules('subsidi_spp', 'Subsidi SPP', 'required|numeric');
        $this->form_validation->set_rules('lain_lain', 'Lain lain', 'required|numeric');
        $this->form_validation->set_rules('potongan_bpjs', 'Potongan BPJS', 'required|numeric');
        $this->form_validation->set_rules('potongan_absensi', 'Potongan Absensi', 'required|numeric');
        $this->form_validation->set_rules('potongan_spp_anak', 'Potongan SPP Anak', 'required|numeric');
        $this->form_validation->set_rules('potongan_angsuran_sekolah', 'Potongan Angsuran Sekolah', 'required|numeric');
        $this->form_validation->set_rules('potongan_pinjaman', 'Potongan Pinjaman', 'required|numeric');
        $this->form_validation->set_rules('potongan_pgri', 'Potongan PGRI', 'required|numeric');
        $this->form_validation->set_rules('total_penerimaan', 'Total Penerimaan', 'required|numeric');
        $this->form_validation->set_rules('total_potongan', 'Total Potongan', 'required|numeric');
        $this->form_validation->set_rules('total', 'Total Bersih', 'required|numeric');
        $this->form_validation->set_rules('periode', 'Periode', 'required');

        if ($this->form_validation->run() == false) {

			$this->load->view('template/header');
            $this->load->view('template/menu');
            $this->load->view('template/topMenu');
            $this->load->view('pages/main/edit_gaji', $data);
            $this->load->view('template/footer');
        
        } else {

            $this->gaji_model->edit_gaji();
			$this->session->set_flashdata('gaji', '<div class="alert alert-success" role="alert">Data Penggajian Berhasil di edit .</div>');
			redirect("penggajian/view/".$encode);
        }

    }

    public function gaji_pdf($periode)
    { 
        $data['pegawai'] = $this->gaji_model->laporan_pdf($periode);

        $this->load->library('pdf');

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->filename = "laporan-semua-gaji-karyawan.pdf";
		$this->pdf->load_view('laporan/all_gaji', $data);
    }

    public function semuagaji(){

        $data['pegawai'] = $this->gaji_model->pdf();

        $this->load->library('pdf');

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->filename = "laporan-semua-gaji-karyawan.pdf";
		$this->pdf->load_view('laporan/all_gaji', $data);
    }

    public function view_detail_pdf($id)
    {
        $nik = base64_decode($id);
		$data['alldata'] = $this->gaji_model->view_detail($nik);

		$this->load->library('pdf');

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->filename = "laporan-detail-gaji.pdf";
		$this->pdf->load_view('laporan/detail_gaji', $data);
    }
}
