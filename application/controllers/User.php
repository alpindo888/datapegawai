<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    private $role;
    
    function __construct() {
		parent::__construct();
        $this->load->model('user_model');
        $this->role = $this->session->userdata("role");
        if(!$this->role){
        
            redirect("login");
        
        } else {

            if($this->role == "user"){
                
                redirect("home");
            }
        }
    }

    public function index()
    {

        $user = [];
        $admin = [];
        $superadmin = [];
        $users = $this->user_model->get_user();
        $data["users"] = $users;

        if(count($users) != 0){
            foreach($users as $key => $value){
                if($value['level'] == "super_admin"){
                    array_push($superadmin, $value);
                } else if ($value['level'] == "admin"){
                    array_push($admin, $value);
                } else {
                    array_push($user, $value);
                }
            }
        }
        $data['email'] = $this->session->userdata('email');
        $data['user'] = $user;
        $data['admin'] = $admin;
        $data['superadmin'] = $superadmin;
        $data['title'] = "user setting";
        $data['role'] = $this->role;

        $this->load->view('template/header', $data);
        $this->load->view('template/menu');
        $this->load->view('template/topMenu');
        $this->load->view('pages/data_user/index');
        $this->load->view('template/footer');
    }

    public function tambah_user(){

        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[tbl_user.email]', [
            'is_unique' => 'This email has already registered!'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]', [
            'matches' => 'Password dont match!',
            'min_length' => 'Password too short!'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

        if ($this->form_validation->run() == false) {

            $user = [];
            $admin = [];
            $superadmin = [];
            $users = $this->user_model->get_user();
            $data["users"] = $users;
            
            if(count($users) != 0){
                foreach($users as $key => $value){
                    if($value['level'] == "super_admin"){
                        array_push($superadmin, $value);
                    } else if ($value['level'] == "admin"){
                        array_push($admin, $value);
                    } else {
                        array_push($user, $value);
                    }
                }
            }
           
            $data['email'] = $this->session->userdata('email');
            $data['user'] = $user;
            $data['admin'] = $admin;
            $data['superadmin'] = $superadmin;

            $this->load->view('template/header');
            $this->load->view('template/menu', $data);
            $this->load->view('template/topMenu', $data);
            $this->load->view('pages/data_user/tambah_user', $data);
            $this->load->view('template/footer');
        } else {
            $email = $this->input->post('email', true);
            $data = [
                'email' => htmlspecialchars($email),
                'password' => md5($this->input->post("password1")),
                'level' => htmlspecialchars($this->input->post('level')),
                'date_created' =>htmlspecialchars($this->input->post('date_created'))
            ];

            $this->db->insert('tbl_user', $data);

            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Akun berhasil ditambah.</div>');
            redirect('user/index');
        }
    }

    public function delete_user($id){

        $this->user_model->delete_user($id);
        $this->session->set_flashdata('delete', '<div class="alert alert-danger" role="alert">Akun berhasil dihapus !</div>');
        redirect('user/index');

    }

    public function edit_user($id){


        $data['userById'] = $this->user_model->get_user_id($id);

        $data['level'] = ['user', 'admin', 'super_admin'];

        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]', [
            'min_length' => 'Password too short!'
        ]);

        if ($this->form_validation->run() == false) {
            $user = [];
            $admin = [];
            $superadmin = [];
            $users = $this->user_model->get_user();
            $data["users"] = $users;
            
            if(count($users) != 0){
                foreach($users as $key => $value){
                    if($value['level'] == "super_admin"){
                        array_push($superadmin, $value);
                    } else if ($value['level'] == "admin"){
                        array_push($admin, $value);
                    } else {
                        array_push($user, $value);
                    }
                }
            }
           
            $data['email'] = $this->session->userdata('email');
            $data['user'] = $user;
            $data['admin'] = $admin;
            $data['superadmin'] = $superadmin;

            $this->load->view('template/header');
            $this->load->view('template/menu', $data);
            $this->load->view('template/topMenu', $data);
            $this->load->view('pages/data_user/edit_user', $data);
            $this->load->view('template/footer');
        } else {

            $this->user_model->edit_user();
            $this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Akun Berhasil di edit.</div>');
            redirect('user/index');
        }
    }

    public function laporan()
    {
        $data['alldata'] = $this->user_model->get_user();

        $this->load->library('pdf');

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->filename = "laporan-semua-user.pdf";
		$this->pdf->load_view('laporan/user', $data);

    }
}