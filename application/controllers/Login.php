<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    function __construct() {
		parent::__construct();
		$this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('user_model');
    }
    
	public function index()
	{
        $data['title'] = "Login Data pegawai";
		$this->load->view('template/login-header', $data);
		$this->load->view('pages/login/login');
		$this->load->view('template/login-footer');
    }
    
    public function check_auth()
    {
        $email = $this->input->post('email');
        $pass  = md5($this->input->post('password'));
        $validation = $this->user_model->auth($email, $pass);

        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('msg', 'format username atau password anda salah !');
            redirect('login');
        } else {
            if($validation != false){
                $this->session->set_userdata('login', true);
                $this->session->set_userdata('role', $validation->level);
                $this->session->set_userdata('user', array(
                    'email' => $validation->email
                ));
                redirect('karyawan');
            } else {
                $this->session->set_flashdata('msg', 'username atau password anda salah !');
                redirect('login');
            }
        }
    }

    public function exit()
    {
        $this->session->sess_destroy();
        redirect('karyawan');
    }
}
