<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {
	private $role;
	
	function __construct() {
		parent::__construct();
		$this->load->model("pegawai_model");
		$this->load->library('session');
		$this->role = $this->session->userdata("role");

		if(!$this->role){
        
            redirect("login");
        
        }
	}

	public function index()
	{

		if($this->role == "user"){
			$identity = $this->session->userdata("user")['email'];
			$datapegawai = $this->pegawai_model->dataPegawai();
			$array = [];

			foreach($datapegawai as $value){
				if($value['email'] == $identity ){
					array_push($array, $value);
				}
			}
			
			$data['pegawai'] = $array;

		} else {

			$data['pegawai'] = $this->pegawai_model->dataPegawai();
		}

		$data['role'] = $this->role;

        $this->load->view('template/header');
        $this->load->view('template/menu');
        $this->load->view('template/topMenu');
        $this->load->view('pages/main/karyawan', $data);
        $this->load->view('template/footer');
	}

	public function tambah()
	{	

		// data pribadi
		$this->form_validation->set_rules('kode_pegawai', 'Kode Pegawai', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('nuptk', 'NUPTK', 'is_unique[tbl_pribadi.nuptk]', [
            'is_unique' => 'NUPTK sudah terdaftar!'
		]);
		$this->form_validation->set_rules('nik', 'NIK', 'required|is_unique[tbl_pribadi.nik]', [
            'is_unique' => 'NIK sudah terdaftar!'
        ]);
		$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
		$this->form_validation->set_rules('status_kepegawaian', 'Status Kepegawaian', 'required');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
		$this->form_validation->set_rules('agama', 'Agama', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('no_hp', 'No HP', 'required|numeric');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tbl_pribadi.email]', [
            'is_unique' => 'This email has already registered!'
		]);
		$this->form_validation->set_rules('tmt', 'TMT', 'required');
		$this->form_validation->set_rules('nama_ibu_kandung', 'Nama Ibu Kandung', 'required');
		$this->form_validation->set_rules('status_perkawinan', 'Status Perkawinan', 'required');
		$this->form_validation->set_rules('nama_suami_istri', 'Nama Suami / Istri', 'required');
		$this->form_validation->set_rules('jumlah_anak', 'Jumlah Anak', 'required');
		$this->form_validation->set_rules('jumlah_saudara', 'Jumlah Saudara', 'required');
		$this->form_validation->set_rules('pekerjaan_suami_istri', 'Pekerjaan Suami / Istri', 'required');
		$this->form_validation->set_rules('npwp', 'NPWP', 'required');
		$this->form_validation->set_rules('nama_wajib_pajak', 'Nama Wajib Pajak', 'required');
		$this->form_validation->set_rules('kewarganegaraan', 'Kewarganegaraan', 'required');

		// data keluarga
		$this->form_validation->set_rules('nama_suami', 'Nama Suami / Istri', 'required');
		$this->form_validation->set_rules('tempat_lahir_suami', 'Tempat Lahir Suami / Istri', 'required');
		$this->form_validation->set_rules('pekerjaan_suami', 'Pekerjaan Suami / Istri', 'required');
		$this->form_validation->set_rules('nama_ibu', 'Nama Orang Tua', 'required');
		$this->form_validation->set_rules('tempat_lahir_ibu', 'Tempat Lahir Orang Tua', 'required');
		$this->form_validation->set_rules('pekerjaan_ibu', 'Pekerjaan Orang Tua', 'required');
		$this->form_validation->set_rules('nama_ayah', 'Nama ayah', 'required');
		$this->form_validation->set_rules('tempat_lahir_ayah', 'Tempat Lahir ayah', 'required');
		$this->form_validation->set_rules('pekerjaan_ayah', 'Pekerjaan ayah', 'required');

		// data riwayat kerja
		// $this->form_validation->set_rules('dari_bulan_tahun', 'Dari', 'required');
		// $this->form_validation->set_rules('sampai_bulan_tahun', 'sampai', 'required');
		// $this->form_validation->set_rules('nama_perusahaan', 'Nama Perusahaan', 'required');
		// $this->form_validation->set_rules('alamat_perusahaan', 'Alamat Perusahaan', 'required');

		//data pendidikan
		$this->form_validation->set_rules('jenjang', 'Jenjang', 'required');
		$this->form_validation->set_rules('nama_sekolah_universitas', 'Nama Sekolah / Universitas', 'required');
		$this->form_validation->set_rules('jurusan', 'Jurusan', 'required');
		$this->form_validation->set_rules('tahun_masuk', 'Tahun Masuk', 'required|numeric');
		$this->form_validation->set_rules('tahun_lulus', 'Tahun Lulus', 'required|numeric');

		if ($this->form_validation->run() == false) {
			$kode = $this->pegawai_model->pegawai();
			$kodePegawai = $kode[0]['terbesar'];
			$urutan = (int) substr($kodePegawai, 1, 3);
			$urutan++;
			$huruf = "P";
			$data['kodePegawai'] = $huruf . sprintf("%03s", $urutan);
			$data['role'] = $this->role;
			$data['email'] = $this->session->userdata("user")['email'];

			$this->load->view('template/header');
			$this->load->view('template/menu');
			$this->load->view('template/topMenu');
			$this->load->view('pages/main/tambah_pegawai', $data);
			$this->load->view('template/footer');
		} else {
			$upload_image = $_FILES['image']['name'];

            if ($upload_image) {
                $config['allowed_types'] = 'jpeg|jpg|png';
                $config['max_size']      = '2048';
                $config['upload_path'] = './assets/img/img_user/';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {

					$datapribadi = [
						'kode_pegawai' => htmlspecialchars($this->input->post('kode_pegawai')),
						'nama' => htmlspecialchars($this->input->post('nama')),
						'nuptk' => htmlspecialchars($this->input->post('nuptk')),
						'nik' => htmlspecialchars($this->input->post('nik')),
						'jenis_kelamin' => htmlspecialchars($this->input->post('jenis_kelamin')),
						'tempat_lahir' => htmlspecialchars($this->input->post('tempat_lahir')),
						'tanggal_lahir' => htmlspecialchars($this->input->post('tanggal_lahir')),
						'status_kepegawaian' => htmlspecialchars($this->input->post('status_kepegawaian')),
						'jabatan' => htmlspecialchars($this->input->post('jabatan')),
						'agama' => htmlspecialchars($this->input->post('agama')),
						'alamat' => htmlspecialchars($this->input->post('alamat')),
						'no_hp' => htmlspecialchars($this->input->post('no_hp')),
						'email' => htmlspecialchars($this->input->post('email')),
						'tmt' => htmlspecialchars($this->input->post('tmt')),
						'nama_ibu_kandung' => htmlspecialchars($this->input->post('nama_ibu_kandung')),
						'status_perkawinan' => htmlspecialchars($this->input->post('status_perkawinan')),
						'nama_suami_istri' => htmlspecialchars($this->input->post('nama_suami_istri')),
						'pekerjaan_suami_istri' => htmlspecialchars($this->input->post('pekerjaan_suami_istri')),
						'jumlah_anak' => htmlspecialchars($this->input->post('jumlah_anak')),
						'jumlah_saudara' => htmlspecialchars($this->input->post('jumlah_saudara')),
						'npwp' => htmlspecialchars($this->input->post('npwp')),
						'nama_wajib_pajak' => htmlspecialchars($this->input->post('nama_wajib_pajak')),
						'kewarganegaraan' => htmlspecialchars($this->input->post('kewarganegaraan')),
						'image' => htmlspecialchars($this->upload->data('file_name'))
					];
		
					$datakeluarga = [
						'nik' => htmlspecialchars($this->input->post('nik')),
						'nama_suami' => htmlspecialchars($this->input->post('nama_suami')),
						'tempat_lahir_suami' => htmlspecialchars($this->input->post('tempat_lahir_suami')),
						'tanggal_lahir_suami' => htmlspecialchars($this->input->post('tanggal_lahir_suami')),
						'pekerjaan_suami' => htmlspecialchars($this->input->post('pekerjaan_suami')),
						'nama_ibu' => htmlspecialchars($this->input->post('nama_ibu')),
						'tempat_lahir_ibu' => htmlspecialchars($this->input->post('tempat_lahir_ibu')),
						'tanggal_lahir_ibu' => htmlspecialchars($this->input->post('tanggal_lahir_ibu')),
						'pekerjaan_ibu' => htmlspecialchars($this->input->post('pekerjaan_ibu')),
						'nama_ayah' => htmlspecialchars($this->input->post('nama_ayah')),
						'tempat_lahir_ayah' => htmlspecialchars($this->input->post('tempat_lahir_ayah')),
						'tanggal_lahir_ayah' => htmlspecialchars($this->input->post('tanggal_lahir_ayah')),
						'pekerjaan_ayah' => htmlspecialchars($this->input->post('pekerjaan_ayah'))
		
					];
		
					$datariwayatpekerjaan = [
						'nik' => htmlspecialchars($this->input->post('nik')),
						'dari_bulan_tahun' => htmlspecialchars($this->input->post("dari_bulan_tahun")),
						'sampai_bulan_tahun' => htmlspecialchars($this->input->post("sampai_bulan_tahun")),
						'nama_perusahaan' => htmlspecialchars($this->input->post("nama_perusahaan")),
						'alamat_perusahaan' => htmlspecialchars($this->input->post("alamat_perusahaan"))
					];
		
					$datapendidikan = [
						'nik' => htmlspecialchars($this->input->post('nik')),
						'jenjang' => htmlspecialchars($this->input->post('jenjang')),
						'nama_sekolah_universitas' => htmlspecialchars($this->input->post('nama_sekolah_universitas')),
						'fakultas' => htmlspecialchars($this->input->post('fakultas')),
						'jurusan' => htmlspecialchars($this->input->post('jurusan')),
						'tahun_masuk' => htmlspecialchars($this->input->post('tahun_masuk')),
						'tahun_lulus' => htmlspecialchars($this->input->post('tahun_lulus'))
					];

                    $this->db->insert('tbl_pribadi', $datapribadi);
					$this->db->insert('tbl_keluarga', $datakeluarga);
					$this->db->insert('tbl_riwayatpekerjaan', $datariwayatpekerjaan);
					$this->db->insert('tbl_pendidikan', $datapendidikan);

					$this->session->set_flashdata('success', '<div class="alert alert-success" role="alert">Data Berhasil Di tambahkan !</div>');
					redirect('karyawan/index');

                } else {
                    echo $this->upload->display_errors();
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Silahkan pilih file untuk upload bukti pembayaran .</div>');
                redirect('karyawan/tambah');
            }


		}
	}

	public function view($nik)
	{
	

		$id = base64_decode($nik);
		$data['role'] = $this->role;
		$data['query'] = $this->pegawai_model->view_detail($id);



		$this->load->view('template/header');
		$this->load->view('template/menu');
		$this->load->view('template/topMenu');
		$this->load->view('pages/main/view_pegawai', $data);
		$this->load->view('template/footer');
	}

	public function edit_data_pribadi($id)
	{

		$nik = base64_decode($id);
		$data['role'] = $this->session->userdata("role");
		$data['jenis_kelamin'] = ['Laki-laki', 'Perempuan'];
		$data['status_kepegawaian'] = ['GTY', 'GTT', 'Honor'];
		$data['jabatan'] = ['Guru', 'Kepala Sekolah', 'Tenaga Administrasi', 'Keamanan', 'Tenaga Kebersihan'];
		$data['getdata'] = $this->pegawai_model->get_pribadi_id($nik);
		$data['keluarga'] = $this->pegawai_model->get_keluarga_id($nik);
		$data['pekerjaan'] = $this->pegawai_model->get_pekerjaan_id($nik);
		$data['pendidikan'] = $this->pegawai_model->get_pendidikan_id($nik);

		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('kode_pegawai', 'Kode Pegawai', 'required');
		$this->form_validation->set_rules('nuptk', 'NUPTK', 'required');
		$this->form_validation->set_rules('nik', 'NIK', 'required');
		$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
		$this->form_validation->set_rules('status_kepegawaian', 'Status Kepegawaian', 'required');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
		$this->form_validation->set_rules('agama', 'Agama', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('no_hp', 'No HP', 'required|numeric');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('tmt', 'TMT', 'required');
		$this->form_validation->set_rules('nama_ibu_kandung', 'Nama Ibu Kandung', 'required');
		$this->form_validation->set_rules('status_perkawinan', 'Status Perkawinan', 'required');
		$this->form_validation->set_rules('nama_suami_istri', 'Nama Suami / Istri', 'required');
		$this->form_validation->set_rules('pekerjaan_suami_istri', 'Pekerjaan Suami / Istri', 'required');
		$this->form_validation->set_rules('jumlah_anak', 'Jumlah Anak', 'required');
		$this->form_validation->set_rules('jumlah_saudara', 'Jumlah Saudara', 'required');
		$this->form_validation->set_rules('npwp', 'NPWP', 'required');
		$this->form_validation->set_rules('nama_wajib_pajak', 'Nama Wajib Pajak', 'required');
		$this->form_validation->set_rules('kewarganegaraan', 'Kewarganegaraan', 'required');

		if ($this->form_validation->run() == false) {

			$this->load->view('template/header');
			$this->load->view('template/menu');
			$this->load->view('template/topMenu');
			$this->load->view('pages/main/edit_data_pribadi', $data);
			$this->load->view('template/footer');
		} else {
			// cek jika ada gambar yang akan diupload
            $upload_image = $_FILES['image']['name'];

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size']      = '2048';
                $config['upload_path'] = './assets/img/img_user/';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {
                    $old_image = $data['user']['image'];
                    if ($old_image != 'default.jpg') {
                        unlink(FCPATH . 'assets/img/img_user/' . $old_image);
                    }
                    $new_img = $this->upload->data('file_name');    
                    $new_image = str_replace('_', ' ', $new_img);

                    
                } else {
                    echo $this->upload->dispay_errors();
                }
            }
			$encode = base64_encode($this->input->post('nik'));
			$this->pegawai_model->edit_data_pribadi($new_image);
			$this->session->set_flashdata('pribadi', '<div class="alert alert-success" role="alert">Data Pribadi Berhasil di edit .</div>');
			redirect("karyawan/view/".$encode);
		}
	}

	public function edit_data_keluarga($id)
	{
		$nik = base64_decode($id);
		$encode = base64_encode($nik);
		$data['getdata'] = $this->pegawai_model->get_keluarga_id($nik);

		$this->form_validation->set_rules('nama_suami', 'Nama Suami / Istri', 'required');
		$this->form_validation->set_rules('tempat_lahir_suami', 'Tempat Lahir Suami / Istri', 'required');
		$this->form_validation->set_rules('pekerjaan_suami', 'Pekerjaan Suami / Istri', 'required');
		$this->form_validation->set_rules('nama_ibu', 'Nama Ibu', 'required');
		$this->form_validation->set_rules('tempat_lahir_ibu', 'Tempat Lahir Ibu', 'required');
		$this->form_validation->set_rules('pekerjaan_ibu', 'Pekerjaan Ibu', 'required');
		$this->form_validation->set_rules('nama_ayah', 'Nama ayah', 'required');
		$this->form_validation->set_rules('tempat_lahir_ayah', 'Tempat Lahir ayah', 'required');
		$this->form_validation->set_rules('pekerjaan_ayah', 'Pekerjaan ayah', 'required');

		if ($this->form_validation->run() == false) {

			$this->load->view('template/header');
			$this->load->view('template/menu');
			$this->load->view('template/topMenu');
			$this->load->view('pages/main/edit_data_keluarga', $data);
			$this->load->view('template/footer');
		} else {
			$this->pegawai_model->edit_data_keluarga();
			$this->session->set_flashdata('keluarga', '<div class="alert alert-success" role="alert">Data Keluarga Berhasil di edit .</div>');
			redirect("karyawan/view/".$encode);
		}
	}

	public function edit_data_pekerjaan($id)
	{
		$nik = base64_decode($id);
		$encode = base64_encode($nik);
		$data['getdata'] = $this->pegawai_model->get_pekerjaan_id($nik);

		// $this->form_validation->set_rules('dari_bulan_tahun', 'Dari', 'required');
		// $this->form_validation->set_rules('sampai_bulan_tahun', 'sampai', 'required');
		$this->form_validation->set_rules('nama_perusahaan', 'Nama Perusahaan', 'required');
		$this->form_validation->set_rules('alamat_perusahaan', 'Alamat Perusahaan', 'required');

		if ($this->form_validation->run() == false) {

			$this->load->view('template/header');
			$this->load->view('template/menu');
			$this->load->view('template/topMenu');
			$this->load->view('pages/main/edit_data_pekerjaan', $data);
			$this->load->view('template/footer');
		} else {
			$this->pegawai_model->edit_data_pekerjaan();
			$this->session->set_flashdata('pekerjaan', '<div class="alert alert-success" role="alert">Data Riwayat Pekerjaan Berhasil di edit .</div>');
			redirect("karyawan/view/".$encode);
		}
	}

	public function delete_karyawan($id){

		$nik = base64_decode($id);

        $this->pegawai_model->delete_pegawai($nik);
        $this->session->set_flashdata('delete', '<div class="alert alert-danger" role="alert">Data Pegawai Berhasil di hapus</div>');
        redirect('karyawan/index');

	}
	
	public function detail_karyawan_pdf($id)
	{
		$nik = base64_decode($id);
		$data['alldata'] = $this->pegawai_model->view_detail($nik);

		$this->load->library('pdf');

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->filename = "laporan-detail-karyawan.pdf";
		$this->pdf->load_view('laporan/detail_karyawan', $data);
	}

	public function all_karyawan_pdf()
	{
		$data['pegawai'] = $this->pegawai_model->dataPegawai();

		$this->load->library('pdf');

		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->filename = "laporan-semua-karyawan.pdf";
		$this->pdf->load_view('laporan/all_karyawan', $data);
	}

	public function edit_data_pendidikan($id)
	{
		$nik = base64_decode($id);
		$encode = base64_encode($nik);
		$data['getdata'] = $this->pegawai_model->get_pendidikan_id($nik);

		//data pendidikan
		$this->form_validation->set_rules('jenjang', 'Jenjang', 'required');
		$this->form_validation->set_rules('nama_sekolah_universitas', 'Nama Sekolah / Universitas', 'required');
		$this->form_validation->set_rules('jurusan', 'Jurusan', 'required');
		$this->form_validation->set_rules('tahun_masuk', 'Tahun Masuk', 'required|numeric');
		$this->form_validation->set_rules('tahun_lulus', 'Tahun Lulus', 'required|numeric');
		

		if ($this->form_validation->run() == false) {

			$this->load->view('template/header');
			$this->load->view('template/menu');
			$this->load->view('template/topMenu');
			$this->load->view('pages/main/edit_data_pendidikan', $data);
			$this->load->view('template/footer');
		} else {
			$this->pegawai_model->edit_data_pendidikan();
			$this->session->set_flashdata('pendidikan', '<div class="alert alert-success" role="alert">Data Pendidikan Berhasil di edit .</div>');
			redirect("karyawan/view/".$encode);
		}
	}

}
