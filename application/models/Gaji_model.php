<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gaji_model extends CI_Model {

    public function dataPegawai()
    {
        $this->db->select("tbl_pribadi.email, tbl_pribadi.nama, tbl_pribadi.nik,tbl_gaji.id_gaji, tbl_gaji.total, tbl_gaji.periode");
        $this->db->from("tbl_pribadi");
        $this->db->join("tbl_gaji", "tbl_pribadi.nik = tbl_gaji.nik");
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function dataPegawai_user($email)
    {
        $this->db->select("tbl_pribadi.email, tbl_pribadi.nama, tbl_pribadi.nik, tbl_gaji.total");
        $this->db->from("tbl_pribadi");
        $this->db->join("tbl_gaji", "tbl_pribadi.nik = tbl_gaji.nik");
        $this->db->where("tbl_pribadi.email", $email);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function delete_gaji($id)
    {
        $this->db->delete('tbl_gaji', ['id_gaji' => $id]);
    }

    public function view_detail($nik)
    {
        $this->db->select("*");
        $this->db->from("tbl_pribadi");
        $this->db->join("tbl_gaji", "tbl_pribadi.nik = tbl_gaji.nik");
        $this->db->where("tbl_pribadi.nik", $nik);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function edit_gaji()
    {
        $data = [
            'nik' => htmlspecialchars($this->input->post('nik')),
            'gaji_pokok' => htmlspecialchars($this->input->post('gaji_pokok')),
            'struktural' => htmlspecialchars($this->input->post('struktural')),
            'fungsional' => htmlspecialchars($this->input->post('fungsional')),
            'utilities' => htmlspecialchars($this->input->post('utilities')),
            'jumlah_hari_masuk' => htmlspecialchars($this->input->post('jumlah_hari_masuk')),
            'transport' => htmlspecialchars($this->input->post('transport')),
            'jumlah_transport' => htmlspecialchars($this->input->post('jumlah_transport')),
            'subsidi_bpjs' => htmlspecialchars($this->input->post('subsidi_bpjs')),
            'inval' => htmlspecialchars($this->input->post('inval')),
            'subsidi_spp' => htmlspecialchars($this->input->post('subsidi_spp')),
            'lain_lain' => htmlspecialchars($this->input->post('lain_lain')),
            'potongan_bpjs' => htmlspecialchars($this->input->post('potongan_bpjs')),
            'potongan_absensi' => htmlspecialchars($this->input->post('potongan_absensi')),
            'potongan_spp_anak' => htmlspecialchars($this->input->post('potongan_spp_anak')),
            'potongan_angsuran_sekolah' => htmlspecialchars($this->input->post('potongan_angsuran_sekolah')),
            'potongan_pinjaman' => htmlspecialchars($this->input->post('potongan_pinjaman')),
            'potongan_pgri' => htmlspecialchars($this->input->post('potongan_pgri')),
            'total_penerimaan' => htmlspecialchars($this->input->post('total_penerimaan')),
            'total_potongan' => htmlspecialchars($this->input->post('total_potongan')),
            'total' => htmlspecialchars($this->input->post('total')),
            'periode' => htmlspecialchars($this->input->post('periode'))
        ];

        $this->db->where('id_gaji', $this->input->post('id_gaji'));
        $this->db->update('tbl_gaji', $data);
    }

    public function laporan_pdf($periode)
    {
        $this->db->select("tbl_pribadi.email, tbl_pribadi.nama, tbl_pribadi.nik,tbl_gaji.id_gaji, tbl_gaji.total, tbl_gaji.periode");
        $this->db->from("tbl_pribadi");
        $this->db->join("tbl_gaji", "tbl_pribadi.nik = tbl_gaji.nik");
        $this->db->where('periode', $periode);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function pdf()
    {
        $this->db->select("tbl_pribadi.email, tbl_pribadi.nama, tbl_pribadi.nik,tbl_gaji.id_gaji, tbl_gaji.total, tbl_gaji.periode");
        $this->db->from("tbl_pribadi");
        $this->db->join("tbl_gaji", "tbl_pribadi.nik = tbl_gaji.nik");
        $query = $this->db->get()->result_array();
        return $query;
    }
}