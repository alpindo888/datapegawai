<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public function auth($email, $pass)
    {
        $this->db->where('email', $email);
        $user_email = $this->db->get('tbl_user')->row();
        if($user_email && $user_email->password == $pass){
            return $user_email;
        } else {
            return false;
        }
    }

	public function get_user()
	{
		return $this->db->get('tbl_user')->result_array();
    }
    
    public function delete_user($id)
    {
        $this->db->delete('tbl_user', ['id_user' => $id]);
    }

    public function get_user_id($id)
    {
        return $this->db->get_where('tbl_user', ['id_user' => $id])->row_array();
    }

    public function edit_user(){
        $email = $this->input->post('email', true);
        $data = [
            'email' => htmlspecialchars($email),
            'password' => md5($this->input->post('password1')),
            'level' => htmlspecialchars($this->input->post('level')),
            'date_created' =>htmlspecialchars($this->input->post('date_created'))
        ];
        
        $this->db->where('id_user', $this->input->post('id_user'));
        $this->db->update('tbl_user', $data);
    }

    
}
