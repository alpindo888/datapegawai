<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai_model extends CI_Model {

    public function dataPegawai()
    {
        return $this->db->get('tbl_pribadi')->result_array();
    }

    public function pegawai(){
        $this->db->select_max('kode_pegawai', 'terbesar');
        $query =  $this->db->get('tbl_pribadi')->result_array();
        return $query;
    }

    public function view_detail($id)
    {
        $this->db->select("*");
        $this->db->from("tbl_pribadi");
        $this->db->join("tbl_keluarga", "tbl_pribadi.nik = tbl_keluarga.nik");
        $this->db->join("tbl_riwayatpekerjaan", "tbl_pribadi.nik = tbl_riwayatpekerjaan.nik");
        $this->db->join("tbl_pendidikan", "tbl_pribadi.nik = tbl_pendidikan.nik");
        $this->db->where("tbl_pribadi.nik", $id);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function get_pribadi_id($nik)
    {
        return $this->db->get_where('tbl_pribadi', ['nik' => $nik])->row_array();
    }

    public function get_keluarga_id($nik)
    {
        return $this->db->get_where('tbl_keluarga', ['nik' => $nik])->row_array();
    }

    public function get_pekerjaan_id($nik)
    {
        return $this->db->get_where('tbl_riwayatpekerjaan', ['nik' => $nik])->row_array();
    }

    public function get_pendidikan_id($nik)
    {
        return $this->db->get_where('tbl_pendidikan', ['nik' => $nik])->row_array();
    }

    public function edit_data_pribadi($new_image)
    {
        $datapribadi = [
            'kode_pegawai' => htmlspecialchars($this->input->post('kode_pegawai')),
            'nama' => htmlspecialchars($this->input->post('nama')),
            'nuptk' => htmlspecialchars($this->input->post('nuptk')),
            'nik' => htmlspecialchars($this->input->post('nik')),
            'jenis_kelamin' => htmlspecialchars($this->input->post('jenis_kelamin')),
            'tempat_lahir' => htmlspecialchars($this->input->post('tempat_lahir')),
            'tanggal_lahir' => htmlspecialchars($this->input->post('tanggal_lahir')),
            'status_kepegawaian' => htmlspecialchars($this->input->post('status_kepegawaian')),
            'jabatan' => htmlspecialchars($this->input->post('jabatan')),
            'agama' => htmlspecialchars($this->input->post('agama')),
            'alamat' => htmlspecialchars($this->input->post('alamat')),
            'no_hp' => htmlspecialchars($this->input->post('no_hp')),
            'email' => htmlspecialchars($this->input->post('email')),
            'tmt' => htmlspecialchars($this->input->post('tmt')),
            'nama_ibu_kandung' => htmlspecialchars($this->input->post('nama_ibu_kandung')),
            'status_perkawinan' => htmlspecialchars($this->input->post('status_perkawinan')),
            'nama_suami_istri' => htmlspecialchars($this->input->post('nama_suami_istri')),
            'pekerjaan_suami_istri' => htmlspecialchars($this->input->post('pekerjaan_suami_istri')),
            'jumlah_anak' => htmlspecialchars($this->input->post('jumlah_anak')),
            'jumlah_saudara' => htmlspecialchars($this->input->post('jumlah_saudara')),
            'npwp' => htmlspecialchars($this->input->post('npwp')),
            'nama_wajib_pajak' => htmlspecialchars($this->input->post('nama_wajib_pajak')),
            'kewarganegaraan' => htmlspecialchars($this->input->post('kewarganegaraan')),
            'image' => $new_image
        ];

        $datakeluarga = [
            'nik' => htmlspecialchars($this->input->post('nik'))
        ];


        $this->db->where('id_pribadi', $this->input->post('id_pribadi'));
        $this->db->update('tbl_pribadi', $datapribadi);

        $this->db->where('id_datakeluarga', $this->input->post('id_datakeluarga'));
        $this->db->update('tbl_keluarga', $datakeluarga);

        $this->db->where('id_pekerjaan', $this->input->post('id_pekerjaan'));
        $this->db->update('tbl_riwayatpekerjaan', $datakeluarga);

        $this->db->where('id_pendidikan', $this->input->post('id_pendidikan'));
        $this->db->update('tbl_pendidikan', $datakeluarga);
    }

    public function edit_data_keluarga()
    {
        $datakeluarga = [
            'nik' => htmlspecialchars($this->input->post('nik')),
            'nama_suami' => htmlspecialchars($this->input->post('nama_suami')),
            'tempat_lahir_suami' => htmlspecialchars($this->input->post('tempat_lahir_suami')),
            'tanggal_lahir_suami' => htmlspecialchars($this->input->post('tanggal_lahir_suami')),
            'pekerjaan_suami' => htmlspecialchars($this->input->post('pekerjaan_suami')),
            'nama_ibu' => htmlspecialchars($this->input->post('nama_ibu')),
            'tempat_lahir_ibu' => htmlspecialchars($this->input->post('tempat_lahir_ibu')),
            'tanggal_lahir_ibu' => htmlspecialchars($this->input->post('tanggal_lahir_ibu')),
            'pekerjaan_ibu' => htmlspecialchars($this->input->post('pekerjaan_ibu')),
            'nama_ayah' => htmlspecialchars($this->input->post('nama_ayah')),
            'tempat_lahir_ayah' => htmlspecialchars($this->input->post('tempat_lahir_ayah')),
            'tanggal_lahir_ayah' => htmlspecialchars($this->input->post('tanggal_lahir_ayah')),
            'pekerjaan_ayah' => htmlspecialchars($this->input->post('pekerjaan_ayah'))

        ];

        $this->db->where('id_datakeluarga', $this->input->post('id_datakeluarga'));
        $this->db->update('tbl_keluarga', $datakeluarga);
    }

    public function edit_data_pekerjaan()
    {
        $datariwayatpekerjaan = [
            'nik' => htmlspecialchars($this->input->post('nik')),
            'dari_bulan_tahun' => htmlspecialchars($this->input->post("dari_bulan_tahun")),
            'sampai_bulan_tahun' => htmlspecialchars($this->input->post("sampai_bulan_tahun")),
            'nama_perusahaan' => htmlspecialchars($this->input->post("nama_perusahaan")),
            'alamat_perusahaan' => htmlspecialchars($this->input->post("alamat_perusahaan"))
        ];

        $this->db->where('id_pekerjaan', $this->input->post('id_pekerjaan'));
        $this->db->update('tbl_riwayatpekerjaan', $datariwayatpekerjaan);
    }

    public function edit_data_pendidikan()
    {
        $datapendidikan = [
            'nik' => htmlspecialchars($this->input->post('nik')),
            'jenjang' => htmlspecialchars($this->input->post('jenjang')),
            'nama_sekolah_universitas' => htmlspecialchars($this->input->post('nama_sekolah_universitas')),
            'fakultas' => htmlspecialchars($this->input->post('fakultas')),
            'jurusan' => htmlspecialchars($this->input->post('jurusan')),
            'tahun_masuk' => htmlspecialchars($this->input->post('tahun_masuk')),
            'tahun_lulus' => htmlspecialchars($this->input->post('tahun_lulus'))
        ];

        $this->db->where('id_pendidikan', $this->input->post('id_pendidikan'));
        $this->db->update('tbl_pendidikan', $datapendidikan);
    }

    public function delete_pegawai($nik)
    {
        $this->db->delete('tbl_pribadi', ['nik' => $nik]);
        $this->db->delete('tbl_keluarga', ['nik' => $nik]);
        $this->db->delete('tbl_riwayatpekerjaan', ['nik' => $nik]);
        $this->db->delete('tbl_pendidikan', ['nik' => $nik]);
    }


}