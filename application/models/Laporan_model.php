<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_model extends CI_Model {

    public function pribadi()
    {
        return $this->db->get('tbl_pribadi')->result();
    }

    public function keluarga()
    {
        return $this->db->get('tbl_keluarga')->result();
    }

    public function pendidikan()
    {
        return $this->db->get('tbl_pendidikan')->result();
    }

    public function riwayatpekerjaan()
    {
        return $this->db->get('tbl_riwayatpekerjaan')->result();
    }

    public function gaji()
    {
        return $this->db->get('tbl_gaji')->result();
    }
}