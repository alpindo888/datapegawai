/*
SQLyog Professional v12.5.1 (64 bit)
MySQL - 5.7.31-0ubuntu0.18.04.1 : Database - pegawai
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pegawai` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pegawai`;

/*Table structure for table `tbl_gaji` */

DROP TABLE IF EXISTS `tbl_gaji`;

CREATE TABLE `tbl_gaji` (
  `id_gaji` int(10) NOT NULL AUTO_INCREMENT,
  `nik` varchar(20) DEFAULT NULL,
  `gaji_pokok` int(10) DEFAULT NULL,
  `struktural` int(10) DEFAULT NULL,
  `fungsional` int(10) DEFAULT NULL,
  `utilities` int(10) DEFAULT NULL,
  `jumlah_hari_masuk` int(10) DEFAULT NULL,
  `transport` int(10) DEFAULT NULL,
  `jumlah_transport` int(10) DEFAULT NULL,
  `subsidi_bpjs` int(10) DEFAULT NULL,
  `inval` int(10) DEFAULT NULL,
  `subsidi_spp` int(10) DEFAULT NULL,
  `lain_lain` int(10) DEFAULT NULL,
  `potongan_bpjs` int(10) DEFAULT NULL,
  `potongan_absensi` int(10) DEFAULT NULL,
  `potongan_spp_anak` int(10) DEFAULT NULL,
  `potongan_angsuran_sekolah` int(10) DEFAULT NULL,
  `potongan_pinjaman` int(10) DEFAULT NULL,
  `potongan_pgri` int(10) DEFAULT NULL,
  `total_penerimaan` int(10) DEFAULT NULL,
  `total_potongan` int(10) DEFAULT NULL,
  `total` int(10) DEFAULT NULL,
  `periode` varchar(10) NOT NULL,
  PRIMARY KEY (`id_gaji`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_gaji` */

insert  into `tbl_gaji`(`id_gaji`,`nik`,`gaji_pokok`,`struktural`,`fungsional`,`utilities`,`jumlah_hari_masuk`,`transport`,`jumlah_transport`,`subsidi_bpjs`,`inval`,`subsidi_spp`,`lain_lain`,`potongan_bpjs`,`potongan_absensi`,`potongan_spp_anak`,`potongan_angsuran_sekolah`,`potongan_pinjaman`,`potongan_pgri`,`total_penerimaan`,`total_potongan`,`total`,`periode`) values 
(11,'3172819392819',3000000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3000000,0,3000000,'08-2020'),
(14,'3172819392819',0,66660000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,66660000,0,66660000,'09-2020'),
(15,'3172819392819',50000000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,50000000,0,50000000,'10-2020'),
(16,'3172819392819',5000000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5000000,0,5000000,'06-2020');

/*Table structure for table `tbl_keluarga` */

DROP TABLE IF EXISTS `tbl_keluarga`;

CREATE TABLE `tbl_keluarga` (
  `id_datakeluarga` int(128) NOT NULL AUTO_INCREMENT,
  `nik` varchar(128) DEFAULT NULL,
  `nama_suami` varchar(128) DEFAULT NULL,
  `tempat_lahir_suami` varchar(128) NOT NULL,
  `tanggal_lahir_suami` varchar(10) DEFAULT NULL,
  `pekerjaan_suami` varchar(128) DEFAULT NULL,
  `nama_ibu` varchar(128) DEFAULT NULL,
  `tempat_lahir_ibu` varchar(128) DEFAULT NULL,
  `tanggal_lahir_ibu` varchar(10) DEFAULT NULL,
  `pekerjaan_ibu` varchar(128) DEFAULT NULL,
  `nama_ayah` varchar(128) DEFAULT NULL,
  `tempat_lahir_ayah` varchar(128) DEFAULT NULL,
  `tanggal_lahir_ayah` varchar(10) DEFAULT NULL,
  `pekerjaan_ayah` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_datakeluarga`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_keluarga` */

insert  into `tbl_keluarga`(`id_datakeluarga`,`nik`,`nama_suami`,`tempat_lahir_suami`,`tanggal_lahir_suami`,`pekerjaan_suami`,`nama_ibu`,`tempat_lahir_ibu`,`tanggal_lahir_ibu`,`pekerjaan_ibu`,`nama_ayah`,`tempat_lahir_ayah`,`tanggal_lahir_ayah`,`pekerjaan_ayah`) values 
(6,'3172819392819','cinta laura','jakarta','1992-01-02','artis','luna maya','jakarta','1992-02-09','artis','joni','jakarta','1992-02-08','nyopet'),
(7,'128310928309128309','shdiuashduih','fuoshfhs','2020-09-28','uashuiahsdui','sdnahsd','sjdaiojsdi','2020-02-10','sdjqoijdqoijd','dqdjqiosjdoiqj','diowjdiojwsdoi','2020-09-21','oaisdjoiasjo');

/*Table structure for table `tbl_pendidikan` */

DROP TABLE IF EXISTS `tbl_pendidikan`;

CREATE TABLE `tbl_pendidikan` (
  `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(128) DEFAULT NULL,
  `jenjang` varchar(128) DEFAULT NULL,
  `nama_sekolah_universitas` varchar(128) DEFAULT NULL,
  `fakultas` varchar(128) DEFAULT NULL,
  `jurusan` varchar(128) DEFAULT NULL,
  `tahun_masuk` varchar(128) DEFAULT NULL,
  `tahun_lulus` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_pendidikan`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_pendidikan` */

insert  into `tbl_pendidikan`(`id_pendidikan`,`nik`,`jenjang`,`nama_sekolah_universitas`,`fakultas`,`jurusan`,`tahun_masuk`,`tahun_lulus`) values 
(2,'3172819392819','smk','tunas harapan','teknik','teknik informatika','2016','2020'),
(3,'128310928309128309','ijwosijqoisdj','wfoiwjfoiwj','wjfiowjfo','wfwoijfoij','2020','2020');

/*Table structure for table `tbl_pribadi` */

DROP TABLE IF EXISTS `tbl_pribadi`;

CREATE TABLE `tbl_pribadi` (
  `id_pribadi` int(128) NOT NULL AUTO_INCREMENT,
  `kode_pegawai` varchar(10) NOT NULL,
  `nama` varchar(128) DEFAULT NULL,
  `nuptk` varchar(128) DEFAULT NULL,
  `nik` varchar(128) DEFAULT NULL,
  `jenis_kelamin` varchar(15) DEFAULT NULL,
  `tempat_lahir` varchar(30) DEFAULT NULL,
  `tanggal_lahir` varchar(10) DEFAULT NULL,
  `status_kepegawaian` varchar(128) DEFAULT NULL,
  `jabatan` varchar(128) DEFAULT NULL,
  `agama` varchar(30) DEFAULT NULL,
  `alamat` varchar(256) DEFAULT NULL,
  `no_hp` varchar(15) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `tmt` varchar(128) DEFAULT NULL,
  `nama_ibu_kandung` varchar(128) DEFAULT NULL,
  `status_perkawinan` varchar(128) DEFAULT NULL,
  `nama_suami_istri` varchar(128) DEFAULT NULL,
  `pekerjaan_suami_istri` varchar(128) DEFAULT NULL,
  `jumlah_anak` varchar(2) DEFAULT NULL,
  `jumlah_saudara` varchar(2) DEFAULT NULL,
  `npwp` varchar(30) DEFAULT NULL,
  `nama_wajib_pajak` varchar(128) DEFAULT NULL,
  `kewarganegaraan` varchar(10) DEFAULT NULL,
  `image` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id_pribadi`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_pribadi` */

insert  into `tbl_pribadi`(`id_pribadi`,`kode_pegawai`,`nama`,`nuptk`,`nik`,`jenis_kelamin`,`tempat_lahir`,`tanggal_lahir`,`status_kepegawaian`,`jabatan`,`agama`,`alamat`,`no_hp`,`email`,`tmt`,`nama_ibu_kandung`,`status_perkawinan`,`nama_suami_istri`,`pekerjaan_suami_istri`,`jumlah_anak`,`jumlah_saudara`,`npwp`,`nama_wajib_pajak`,`kewarganegaraan`,`image`) values 
(6,'P001','alpindoo','31201911','3172819392819','Laki-laki','jakarta','1997-02-09','GTY','Guru','kristen','kali jodo','082112758442','alpindo@gmail.com','2020-01-02','luna maya','kawin','cinta laura','artis','2','2','20183192818728','alpindo','wni',NULL),
(7,'P002','alpoindo','28301928','128310928309128309','Laki-laki','ajdiajdijasdj','1897-12-07','GTY','Guru','siuahsduih','dqushdiuqshdiuqh','289731827891','siahduiah@gmail.com','2020-02-07','sdnahsd','asdhausiohdiua','shdiuashduih','uashuiahsdui','2','2','8273981273','diwhduih','wni','de4ef2e226122a21ca2b234ca8933e57.JPG');

/*Table structure for table `tbl_riwayatpekerjaan` */

DROP TABLE IF EXISTS `tbl_riwayatpekerjaan`;

CREATE TABLE `tbl_riwayatpekerjaan` (
  `id_pekerjaan` int(10) NOT NULL AUTO_INCREMENT,
  `nik` varchar(128) DEFAULT NULL,
  `dari_bulan_tahun` varchar(128) DEFAULT NULL,
  `sampai_bulan_tahun` varchar(128) DEFAULT NULL,
  `nama_perusahaan` varchar(128) DEFAULT NULL,
  `alamat_perusahaan` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id_pekerjaan`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_riwayatpekerjaan` */

insert  into `tbl_riwayatpekerjaan`(`id_pekerjaan`,`nik`,`dari_bulan_tahun`,`sampai_bulan_tahun`,`nama_perusahaan`,`alamat_perusahaan`) values 
(6,'3172819392819','1992-02-08','1993-02-12','az solusindo','jakarta'),
(7,'128310928309128309','2020-02-08','2020-02-09','jsijdoiqjso','qsdoiqjdoiqs');

/*Table structure for table `tbl_user` */

DROP TABLE IF EXISTS `tbl_user`;

CREATE TABLE `tbl_user` (
  `id_user` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `level` varchar(12) NOT NULL,
  `date_created` varchar(50) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tbl_user` */

insert  into `tbl_user`(`id_user`,`email`,`password`,`level`,`date_created`) values 
(6,'superadmin@gmail.com','17c4520f6cfd1ab53d8745e84681eb49','super_admin','11/06/2020'),
(17,'admin@gmail.com','21232f297a57a5a743894a0e4a801fc3','admin','11/07/2020'),
(18,'user@gmail.com','ee11cbb19052e40b07aac0ca060c23ee','user','11/07/2020');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
